({

    initHelper: function (component, event, helper) {
        component.set('v.conditions', []);
        let action = component.get('c.getUrlImages');

        action.setCallback(this, $A.getCallback(function (response) {
            let state = response.getState();
            if (state === 'SUCCESS') component.set('v.url', response.getReturnValue()[2]);
            else if (state === 'ERROR') console.error(response.getError());

            console.log(response.getReturnValue()[0]);
        }));

        $A.enqueueAction(action);
    },
    initSecond: function (component, event, helper) {
        let today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.startDate', today);
        component.set('v.today', today);
        component.set('v.minDate', today);

        helper.getRooms(component, helper);
    },

    getRooms: function (component, helper) {
        let action = component.get('c.getRooms');

        console.log('conditions');
        console.log(component.get('v.typeOfRoom'));

        action.setParams({
            conditions: '',
            typeOfRoom: component.get('v.typeOfRoom'),
            sortBy: component.get('v.sorterBy')
        });

        action.setCallback(this, $A.getCallback(function (response) {

            let state = response.getState();
            if (state === 'SUCCESS') {
                console.log(JSON.parse(response.getReturnValue()));

                component.set("v.totalPages", Math.ceil(JSON.parse(response.getReturnValue()).length / component.get("v.pageSize")));
                component.set("v.allData", JSON.parse(response.getReturnValue()));
                component.set("v.currentPageNumber", 1);

                helper.buildData(component, helper);

                helper.getSpecifications(component, event, helper);
            } else if (state === 'ERROR') console.error(response.getError());
        }));

        $A.enqueueAction(action);
    },

    getSpecifications: function (component, event, helper) {
        let action = component.get('c.getSpecifications');

        action.setCallback(this, $A.getCallback(function (response) {
            let state = response.getState();
            if (state === 'SUCCESS') {
                let data = JSON.parse(response.getReturnValue());
                let accounts = [];
                let floors = [];
                let rooms = [];
                console.log('------------');
                console.log(data);
                console.log(response.getReturnValue());
                for (let i = 0; i < data.length; i++) {
                    if (data[i].sObjectName == 'account') accounts.push(data[i]);
                    else if (data[i].sObjectName == 'floor') floors.push(data[i]);
                    else if (data[i].sObjectName == 'room') rooms.push(data[i]);
                }

                component.set('v.accountsFilter', accounts);
                component.set('v.floorsFilter', floors);
                component.set('v.roomsFilter', rooms);
                console.log('hui');
                console.log(accounts);
                console.log(floors);
                console.log(rooms);

                helper.showFiltersHelper(component, helper);
            } else if (state === 'ERROR') console.error(response.getError());
            console.log(JSON.parse(response.getReturnValue()));
            console.log(JSON.parse(response.getReturnValue())[0].sObjectName);
        }));

        $A.enqueueAction(action);
    },

    createConditionHelper: function (component, event, helper, itemName) {
        console.log(typeof component.get('v.conditions'));
        let conditions;
        console.log(itemName + event.getSource().get("v.name"));
        if (event.getSource().get("v.type") == 'checkbox') {
            // console.log(typeof event.getParam('checked'));
            // console.log(event.getSource().get("v.name") + ' = ' + event.getParam('checked') + ' type: ' + event.getSource().get("v.type"));
            conditions = helper.checkCurrentCondition(component, event, helper, itemName + event.getSource().get("v.name"), event.getSource().get("v.type"), event.getParam('checked'));
        } else if (event.getSource().get("v.type") != 'number') {
            // console.log(typeof event.getParam('checked'));
            // console.log(event.getSource().get("v.name")  + ' = ' + event.getSource().get("v.value") + ' type: ' + event.getSource().get("v.type"));
            conditions = helper.checkCurrentCondition(component, event, helper, itemName + event.getSource().get("v.name"), event.getSource().get("v.type"), event.getSource().get("v.value"));
        } else if (event.getSource().get("v.type") != 'text') {
            // console.log(event.getSource().get("v.name")  + ' = ' + event.getSource().get("v.value") + ' type: ' + event.getSource().get("v.type"));
            conditions = helper.checkCurrentCondition(component, event, helper, itemName + event.getSource().get("v.name"), event.getSource().get("v.type"), event.getSource().get("v.value"));
        }

        let action = component.get('c.getRooms');
        console.log(component.get('v.typeOfRoom'));
        let type = component.get('v.typeOfRoom');
        console.log('conditgion ' + conditions);
        console.log('type ' + type);
        action.setParams({
            conditions: conditions,
            typeOfRoom: type,
            sortBy: ''
        });
        action.setCallback(this, $A.getCallback(function (response) {
            let state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.rooms', JSON.parse(response.getReturnValue()));
            } else if (state === 'ERROR') console.error(response.getError());
            console.log(JSON.parse(response.getReturnValue()));
        }));

        $A.enqueueAction(action);
    },

    searchHelper: function (component, event, helper) {
        console.log(event.target.name);
        console.log(component.get('v.searchEndDate') != undefined);
        console.log(component.get('v.searchEndDate') == undefined);
        console.log(component.get('v.searchEndDate'));
        let oldCondtions = helper.createStringConditions(component, event, helper, component.get('v.conditions'));
        let conditions;
        if (component.get('v.searchStartDate') != undefined) {
            conditions = helper.checkCurrentCondition(component, event, helper, 'searchStartDate', 'text', component.get('v.searchStartDate'));
        }
        if (component.get('v.searchEndDate') != undefined) {
            console.log('enddate');
            console.log(component.get('v.searchEndDate'));
            conditions = helper.checkCurrentCondition(component, event, helper, 'searchEndDate', 'text', component.get('v.searchEndDate'));
        }
        if (component.get('v.searchName') != undefined) {
            conditions = helper.checkCurrentCondition(component, event, helper, 'searchName', 'text', component.get('v.searchName'));
        }
        if (component.get('v.searchAddress') != undefined) {
            conditions = helper.checkCurrentCondition(component, event, helper, 'searchAddress', 'text', component.get('v.searchAddress'));
        }
        if (component.get('v.hourlyWage') != undefined) {
            conditions = helper.checkCurrentCondition(component, event, helper, 'hourlyWage', 'boolean', component.get('v.hourlyWage'));
        }

        // if(conditions != undefined) {
        if (conditions == undefined) conditions = oldCondtions;
        let action = component.get('c.getRooms');
        console.log(component.get('v.typeOfRoom'));
        let type = component.get('v.typeOfRoom');
        console.log('conditgion ' + conditions);
        console.log('type ' + type);
        action.setParams({
            conditions: conditions,
            typeOfRoom: type,
            sortBy: ''
        });
        action.setCallback(this, $A.getCallback(function (response) {
            let state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.rooms', JSON.parse(response.getReturnValue()));
            } else if (state === 'ERROR') console.error(response.getError());
            console.log(JSON.parse(response.getReturnValue()));
        }));
        $A.enqueueAction(action);
        // }
    },

    checkCurrentCondition: function (component, event, helper, fieldName, type, value) {
        let filters = component.get('v.conditions');
        console.log(JSON.parse(JSON.stringify(filters)));
        let isDeleteValue = true;
        for (let i = 0; i < filters.length; i++) {
            if (filters[i].fieldName == fieldName) {
                console.log(filters[i].fieldName == fieldName);
                console.log(typeof value == 'boolean');
                if (type == 'number' && value == '') {
                    filters.splice(i, 1);
                    component.set('v.conditions', filters);
                    console.log(JSON.parse(JSON.stringify(filters)));
                    isDeleteValue = false;
                } else if (type == 'number') {
                    filters.splice(i, 1);
                    component.set('v.conditions', filters);
                    console.log(JSON.parse(JSON.stringify(filters)));
                } else if (type == 'text' && value == '') {
                    console.log('type - text, value - ')
                    filters.splice(i, 1);
                    component.set('v.conditions', filters);
                    console.log(JSON.parse(JSON.stringify(filters)));
                    isDeleteValue = false;
                } else if (type == 'text' && value != '') {
                    filters.splice(i, 1);
                    component.set('v.conditions', filters);
                    console.log(JSON.parse(JSON.stringify(filters)));
                } else if (type == 'date' && value == '') {
                    filters.splice(i, 1);
                    component.set('v.conditions', filters);
                    console.log(JSON.parse(JSON.stringify(filters)));
                    isDeleteValue = false;
                } else if (type == 'date') {
                    filters.splice(i, 1);
                    component.set('v.conditions', filters);
                    console.log(JSON.parse(JSON.stringify(filters)));
                } else if (fieldName == 'hourlyWage' && component.get('v.hourlyWage') == false) {
                    console.log('FIELD SMGTGTGTG');
                    console.log(JSON.parse(JSON.stringify(filters)));
                    filters.splice(i, 1);
                    console.log(JSON.parse(JSON.stringify(filters)));
                    component.set('v.conditions', filters);
                    isDeleteValue = false;
                } else if (fieldName == 'hourlyWage' && component.get('v.hourlyWage') == true) {
                    isDeleteValue = false;
                } else if (typeof value == 'boolean') {
                    console.log('TYPE == BOOLEAN');
                    filters.splice(i, 1);
                    component.set('v.conditions', filters);
                    console.log(JSON.parse(JSON.stringify(filters)));
                    isDeleteValue = false;
                }
            }
        }

        if (isDeleteValue == false) {
            return helper.createStringConditions(component, event, helper, filters);
        }

        if (value !== '') {
            filters.push({
                fieldName: fieldName,
                type: type,
                value: value
            });
        }

        return helper.createStringConditions(component, event, helper, filters);
    },

    createStringConditions: function (component, event, helper, filters) {
        let str = '';
        console.log('my string !!!!!!!! ' + str);

        for (let i = 0; i < filters.length; i++) {
            if (i != 0) str += '%';
            str += filters[i].fieldName + '=' + filters[i].value;

        }
        console.log('my string !!!!!!!! ' + str);
        component.set('v.conditions', filters);
        console.log(JSON.parse(JSON.stringify(filters)));
        console.log(str);
        return str;
    },


    googleNameHelper: function (component, event, helper) {
        let action = component.get('c.autocompleteRequest');
        console.log(component.find('googleName').get('v.value'));
        action.setParams({searchKey: component.find('googleName').get('v.value')});
        action.setCallback(this, $A.getCallback(function (response) {
            let state = response.getState();
            if (state === 'SUCCESS') {
                console.log(JSON.parse(response.getReturnValue()).predictions);
                let data = JSON.parse(response.getReturnValue()).predictions;
                let descriptions = [];
                for (let i = 0; i < data.length; i++) {
                    console.log(data[i]);
                    descriptions.push(data[i].description);
                }
                console.log(descriptions);
                component.set('v.addressPlaces', descriptions);
                component.set('v.countryDropDown', true);
            } else if (state === 'ERROR') console.error(response.getError());
            console.log(JSON.parse(response.getReturnValue()));
        }));

        $A.enqueueAction(action);
    },

    showFiltersHelper: function (component, helper) {
        let size;
        jQuery(document).ready(function () {
            if (component.get('v.accountsFilter').length > 3) {
                $('.filter-section-facilities:gt(2)').hide();
                size = component.get('v.accountsFilter').length - 3;
                $('.show-more-facilities').show();
                $('.show-more-rooms').text('Show more (' + size + ')');
                $('.show-more-facilities').on('click', function () {
                    $('.filter-section-facilities:gt(2)').toggle();
                    $(this).text() === 'Show more (' + size + ')' ? $(this).text('Show less') : $(this).text('Show more (' + size + ')');
                });
            }

            if (component.get('v.floorsFilter').length) {
                $('.filter-section-floors:gt(2)').hide();
                size = component.get('v.floorsFilter').length - 3;
                $('.show-more-floors').show();
                $('.show-more-rooms').text('Show more (' + size + ')');
                $('.show-more-floors').on('click', function () {
                    $('.filter-section-floors:gt(2)').toggle();
                    $(this).text() === 'Show more (' + size + ')' ? $(this).text('Show less') : $(this).text('Show more (' + size + ')');
                });
            }

            if (component.get('v.roomsFilter').length > 3) {
                $('.filter-section-rooms:gt(2)').hide();
                size = component.get('v.roomsFilter').length - 3;
                $('.show-more-rooms').show();
                $('.show-more-rooms').text('Show more (' + size + ')');
                $('.show-more-rooms').on('click', function () {
                    $('.filter-section-rooms:gt(2)').toggle();
                    $(this).text() === 'Show more (1)' ? $(this).text('Show less') : $(this).text('Show more (1)');
                });
            }
        });

    },

    buildData: function (component, helper) {
        let data = [];
        let pageNumber = component.get("v.currentPageNumber");
        let pageSize = component.get("v.pageSize");
        let allData = component.get("v.allData");
        let i = ((pageNumber - 1) * pageSize) + pageNumber - 1;
        let j = (pageNumber * pageSize) + pageNumber - 1;

        for (; i <= j; i++) {
            if (allData[i]) {
                data.push(allData[i]);
            }
        }

        component.set("v.rooms", data);

        helper.generatePageList(component, pageNumber);
    },

    generatePageList: function (component, pageNumber) {
        pageNumber = parseInt(pageNumber);

        let pageList = [];
        let totalPages = component.get("v.totalPages");

        if (totalPages > 1) {
            if (totalPages <= 10) {
                let counter = 2;
                for (; counter < (totalPages); counter++) {
                    pageList.push(counter);
                }
            } else {
                if (pageNumber < 5) {
                    pageList.push(2, 3, 4, 5, 6);
                } else {
                    if (pageNumber > (totalPages - 5)) {
                        pageList.push(totalPages - 5, totalPages - 4, totalPages - 3, totalPages - 2, totalPages - 1);
                    } else {
                        pageList.push(pageNumber - 2, pageNumber - 1, pageNumber, pageNumber + 1, pageNumber + 2);
                    }
                }
            }
        }

        component.set("v.pageList", pageList);
    },

});