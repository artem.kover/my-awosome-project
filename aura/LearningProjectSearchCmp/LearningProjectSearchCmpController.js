({
    doInit : function(component, event, helper) {
        // helper.initHelper(component, event, helper);
        helper.initSecond(component, event, helper);
    },

    createConditionFrontAccount : function(component, event, helper) {
        helper.createConditionHelper(component, event, helper, 'Account');
    },

    createConditionFrontFloor : function(component, event, helper) {
        helper.createConditionHelper(component, event, helper, 'Floor');
    },

    createConditionFrontRoom : function(component, event, helper) {
        helper.createConditionHelper(component, event, helper, 'Room');
    },

    createsearchStartDate : function(component, event, helper) {
        component.set('v.searchStartDate',  component.find('searchStartDate').get('v.value'));
        console.log('men');
        console.log(component.find('searchEndDate').get('v.value') < component.find('searchStartDate').get('v.value')); 
        if(component.find('searchEndDate').get('v.value') < component.find('searchStartDate').get('v.value')) {
            component.find('searchEndDate').set('v.value','');
        }
        console.log(component.find('searchEndDate').get('v.value'));
        console.log(component.find('searchStartDate').get('v.value'));
        console.log(component.find('searchEndDate').get('v.value') == '');
        console.log('>>><<<')
        helper.createConditionHelper(component, event, helper);
    },

    googleNameFront : function(component, event, helper) {
        clearTimeout(component.get('v.timerId'));

        if (component.find('googleName').get('v.value').length> 2) {
            component.set('v.timerId', window.setTimeout(
                $A.getCallback(function() {
                    helper.googleNameHelper(component, event, helper);
                }), 500
            ));
        } else {
            component.set('v.addressPlaces', []);
        }
    },

    selectCountry: function (component, event, helper) {
        let country = event.target.innerText;

        component.set('v.addressValue', country);
        component.set('v.searchAddress', country);
        component.set('v.countryDropDown', false);

        console.log('address');
        console.log(component.get('v.addressValue'));

        console.log(country);
        console.log(component.get('v.country'));
    },



    changeValueFront : function(component, event, helper) {
        console.log(component.find('searchStartDate').get('v.value'));
        console.log(component.find('searchEndDate').get('v.value'));
        if(event.getSource().get('v.name') == 'searchName') {
            component.set('v.searchName', event.getSource().get('v.value'));
        }  else if(event.getSource().get('v.name') == 'searchStartDate') {
            component.set('v.searchStartDate', event.getSource().get('v.value'));
            component.set('v.minDate', component.get('v.searchStartDate'));
            if(component.find('searchEndDate').get('v.value') < component.find('searchStartDate').get('v.value')) {
                component.find('searchEndDate').set('v.value','');
                component.set('v.searchEndDate', '');
            } else if(event.getSource().get('v.value') === null || event.getSource().get('v.value') == '') {
                component.find('searchStartDate').set('v.value','');
                component.set('v.searchStartDate', '');
                component.set('v.minDate', component.get('v.today'));
            }

        } else if(event.getSource().get('v.name') == 'searchEndDate') {
            console.log('check');
            console.log(event.getSource().get('v.value'));
            console.log(event.getSource().get('v.value') == '');
            console.log(event.getSource().get('v.value') == null);
            component.set('v.searchEndDate', event.getSource().get('v.value'));
            if(component.find('searchEndDate').get('v.value') < component.find('searchStartDate').get('v.value')) {
                component.find('searchEndDate').set('v.value','');
            } else if(event.getSource().get('v.value') === null || event.getSource().get('v.value') == '') {
                component.find('searchEndDate').set('v.value','');
                component.set('v.searchEndDate', '');
            }
        } else if(event.getSource().get('v.name') == 'hourlyWage') {
            component.set('v.hourlyWage', event.getParam('checked'));
            // let filters = component.get('v.conditions');
            // for(let i = 0; i < filters.length; i++) {
            //     if(filters[i].fieldName == "searchEndDate") {
            //         filters.splice(i,1);
            //     }
            // }
            // component.set('v.conditions', filters);
            console.log(component.find('searchEndDate').get('v.value'));
            component.find('searchEndDate').set('v.value', '');
            component.set('v.searchEndDate',  '');
            console.log(component.get('v.searchEndDate'));
        }
    },

    searchFront : function(component, event, helper) {
        helper.searchHelper(component, event, helper);
    }, 

    jsLoaded : function(component, event, helper) {
        component.set('v.scriptLoaded', true);
        // helper.showFiltersHelper(component, helper);
        console.log('jsLoader');
        // console.log(component.get('v.doneRendering') == true && component.get('v.scriptLoaded') == true);
        // if(component.get('v.doneRendering') == true && component.get('v.scriptLoaded') == true) {
        //     helper.showFiltersHelper(component, helper);
        // }
        // window.onload = function() {
        //     console.log('document-ready');
        //     helper.showFiltersHelper(component, helper);
        // };
    },

    doneRendering : function(component, event, helper) {
        console.log('DONNENENENENENE');
        component.set('v.doneRendering', true);
        console.log(component.get('v.doneRendering') == true && component.get('v.scriptLoaded') == true);
        if(component.get('v.doneRendering') == true && component.get('v.scriptLoaded') == true) {
            helper.showFiltersHelper(component, helper);
        }
    },

    toggleAside : function(component, event, helper) {
        let showAside = component.get('v.showAside');
        if(showAside == false) {
            component.set('v.showAside', true);
            let aside = component.find('aside');
            $A.util.addClass(aside, 'show-aside');
        } else {
            component.set('v.showAside', false);
            let aside = component.find('aside');
            $A.util.removeClass(aside, 'show-aside');
        }

    },

    onNext : function(component, event, helper) {
        let pageNumber = component.get("v.currentPageNumber");

        component.set("v.currentPageNumber", pageNumber+1);

        helper.buildData(component, helper);
    },

    onPrev : function(component, event, helper) {
        let pageNumber = component.get("v.currentPageNumber");

        component.set("v.currentPageNumber", pageNumber-1);

        helper.buildData(component, helper);
    },

    processMe : function(component, event, helper) {
        component.set("v.currentPageNumber", parseInt(event.target.name));

        helper.buildData(component, helper);
    },

    onFirst : function(component, event, helper) {
        component.set("v.currentPageNumber", 1);

        helper.buildData(component, helper);
    },

    onLast : function(component, event, helper) {
        component.set("v.currentPageNumber", component.get("v.totalPages"));

        helper.buildData(component, helper);
    },

    sortByName: function (component, event, helper) {
        let sortByString = component.get('v.sorterBy');

        if (sortByString.includes('Name') && sortByString.includes('ASC')) {
            sortByString = 'ORDER BY Name DESC';
        } else if (sortByString.includes('Name') && sortByString.includes('DESC')) {
            sortByString = 'ORDER BY Name ASC';
        } else {
            sortByString = 'ORDER BY Name DESC';
        }

        component.set('v.sorterBy', sortByString);
        helper.getRooms(component, helper);
    },

    sortByRating: function (component, event, helper) {
        let sortByString = component.get('v.sorterBy');

        if (sortByString.includes('Average_Rating__c') && sortByString.includes('ASC')) {
            sortByString = 'ORDER BY Average_Rating__c DESC';
        } else if (sortByString.includes('Average_Rating__c') && sortByString.includes('DESC')) {
            sortByString = 'ORDER BY Average_Rating__c ASC';
        } else {
            sortByString = 'ORDER BY Average_Rating__c DESC';
        }

        component.set('v.sorterBy', sortByString);
        helper.getRooms(component, helper);
    },

    sortByPrice: function (component, event, helper) {
        let sortByString = component.get('v.sorterBy');

        if (sortByString.includes('Price__c') && sortByString.includes('ASC')) {
            sortByString = 'ORDER BY Price__c DESC';
        } else if (sortByString.includes('Price__c') && sortByString.includes('DESC')) {
            sortByString = 'ORDER BY Price__c ASC';
        } else {
            sortByString = 'ORDER BY Price__c DESC';
        }

        component.set('v.sorterBy', sortByString);
        helper.getRooms(component, helper);
    },
});