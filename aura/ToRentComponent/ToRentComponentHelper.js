({

    setBuildings: function (component) {

        let action = component.get('c.getBuildings');

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS get buildings');

                let options = [];

                response.getReturnValue().forEach(item => {
                    let object = {};

                    object.id = item.Id;
                    object.Name = item.Name;

                    if (item.BillingAddress) {
                        object.billingAddress = item.BillingAddress.street ? item.BillingAddress.street + ', ' : null;
                        object.billingAddress += item.BillingAddress.city ? item.BillingAddress.city + ', ' : null;
                        object.billingAddress += item.BillingAddress.country ? item.BillingAddress.country : null;
                    }

                    object.floors = item.Floors__r ? item.Floors__r : null;

                    options.push(object);
                });

                component.set('v.buildings', options);

                let currentBuilding = component.get('v.currentBuilding');

                if (currentBuilding !== undefined) {
                    let buildings = component.get('v.buildings');

                    component.set('v.currentBuilding', buildings[component.get('v.currentBuildingNumber')])
                }
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    setRooms: function (component) {
        let action = component.get('c.getRooms');
        let floorId = component.get('v.currentFloor').Id;

        action.setParams({
            floorId: floorId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS set rooms');
                let roomsId = [];
                component.set('v.rooms', response.getReturnValue());

                response.getReturnValue().forEach(item => {
                    roomsId.push(item.Id);
                });

                if (roomsId.length !== 0) {
                    this.setMainPictures(component, roomsId);
                }
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    setMainPictures: function (component, roomsId) {
        let action = component.get('c.getMainPictures');

        action.setParams({
            roomsId: roomsId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS get rooms pictures');
                console.log(JSON.parse(response.getReturnValue()));

                let wrappers = JSON.parse(response.getReturnValue());
                let rooms = component.get('v.rooms');

                rooms.forEach(room => {
                    wrappers.forEach(item => {
                        if (room.Id === item.roomId) {
                            room.picUrl = item.url;
                        }
                    })
                });

                component.set('v.rooms', rooms);
                console.log(component.get('v.rooms'));
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    setBuildingSpecifications: function (component) {

        let action = component.get('c.getBuildingSpecifications');

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS set building roomSpecifications');

                let options = [];

                response.getReturnValue().forEach(item => {
                    options.push({
                        name: item.Name,
                        checked: false
                    });
                });

                component.set('v.buildingSpecifications', options);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    setFloorSpecifications: function (component) {

        let action = component.get('c.getFloorSpecifications');

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS set floors roomSpecifications');

                let options = [];

                response.getReturnValue().forEach(item => {
                    options.push({
                        name: item.Name,
                        checked: false
                    });
                });

                component.set('v.floorSpecifications', options);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    saveBuilding: function (component) {
        if (component.get('v.newBuilding').Name) {

            let action = component.get('c.saveBuildings');
            let building = component.get('v.newBuilding');

            action.setParams({
                building: building
            });

            action.setCallback(this, $A.getCallback(response => {
                if (response.getState() === 'SUCCESS') {
                    console.log('SUCCESS save building');

                    component.set('v.newBuilding.Id', response.getReturnValue());

                    this.saveBuildingSpecifications(component);
                    this.showSuccessToast();
                } else if (response.getState() === 'ERROR') {
                    console.log('ERROR');
                }
            }));

            $A.enqueueAction(action);
        }
    },

    saveBuildingSpecifications: function (component) {
        let allSpecifications = component.get('v.buildingSpecifications');

        let buildingSpecifications = [];

        allSpecifications.forEach(item => {
            if (item.checked === true) {
                buildingSpecifications.push({Name: item.name});
            }
        });

        if (buildingSpecifications.length !== 0) {
            let action = component.get('c.saveSpecifications');

            action.setParams({
                specifications: buildingSpecifications
            });

            action.setCallback(this, $A.getCallback(response => {
                if (response.getState() === 'SUCCESS') {
                    console.log('SUCCESS set save building roomSpecifications');
                    this.saveSpecificationToBuilding(component, response.getReturnValue());
                } else if (response.getState() === 'ERROR') {
                    console.log('ERROR');
                }
            }));

            $A.enqueueAction(action);
        } else {
            component.set('v.newBuilding', {});
            this.clearSpecifications(component);
            this.setBuildings(component);
        }
    },

    saveSpecificationToBuilding: function (component, specificationsToId) {
        let searchedObject = component.get('v.currentBuildingMode');
        let buildingId = component.get('v.' + searchedObject).Id;

        let allSpecifications = searchedObject === 'newBuilding'
            ? component.get('v.buildingSpecifications')
            : component.get('v.editBuildingSpecifications');

        let specificationsToBuilding = [];

        allSpecifications.forEach(item => {
            if (item.checked === true && specificationsToId[item.name]) {
                let object = {};

                object.Account__c = buildingId;
                object.Specification__c = specificationsToId[item.name];

                if (item.type === 'CheckBox') {

                    object.Type__c = item.type;
                    object.Boolean_Value__c = item.value;
                } else if (item.type === 'Number') {

                    object.Type__c = item.type;
                    object.Number_Value__c = item.value;
                } else if (item.type === 'Text') {

                    object.Type__c = item.type;
                    object.Text_Value__c = item.value;
                } else if (item.type === undefined) {

                    object.Type__c = 'CheckBox';
                    object.Boolean_Value__c = true;
                }

                specificationsToBuilding.push(object);
            }
        });

        let action = component.get('c.saveSpecificationsJunction');

        action.setParams({
            specifications: specificationsToBuilding
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS save roomSpecifications to building');

                component.set('v.newBuilding', {});
                this.clearSpecifications(component);
                this.setBuildings(component);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    changeCSSpecifications: function (component) {
        let action = component.get('c.getCSSpecifications');
        let currentMode = component.get('v.currentRoomMode');
        let roomType = component.get('v.' + currentMode).Type__c;

        action.setParams({
            roomType: roomType ? roomType : 'Room'
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS get CS roomSpecifications');

                let options = [];

                response.getReturnValue().forEach(item => {
                    options.push({
                        name: item.Name,
                        checked: false
                    })
                });

                component.set('v.roomSpecifications', options);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    setCountryPlaces: function (component, key) {

        if (key.length > 2) {
            let action = component.get('c.autocompleteRequest');

            action.setParams({
                searchKey: key,
                type: '(cities)'
            });

            action.setCallback(this, $A.getCallback(response => {
                if (response.getState() === 'SUCCESS') {
                    console.log('SUCCESS');

                    console.log(JSON.parse(response.getReturnValue()));

                    let predictions = JSON.parse(response.getReturnValue());
                    let options = [];

                    predictions.predictions.forEach(item => {
                        if (!item.description.includes(',')) {
                            options.push(item.description);
                        }
                    });

                    component.set('v.countryPlaces', options);
                    component.set('v.countryDropDown', true);
                } else if (response.getState() === 'ERROR') {
                    console.error('ERROR');
                }
            }));

            $A.enqueueAction(action);
        }
    },

    setCityPlaces: function (component, key) {

        if (key.length > 2) {
            let action = component.get('c.autocompleteRequest');

            action.setParams({
                searchKey: key,
                type: '(cities)'
            });

            action.setCallback(this, $A.getCallback(response => {
                if (response.getState() === 'SUCCESS') {
                    console.log('SUCCESS');
                    console.log(JSON.parse(response.getReturnValue()));

                    let searchedObject = component.get('v.currentBuildingMode');
                    let predictions = JSON.parse(response.getReturnValue());
                    let options = [];

                    predictions.predictions.forEach(item => {
                        if (item.description.includes(component.get('v.' + searchedObject).BillingCountry)) {
                            options.push(item.description.substring(0, item.description.indexOf(',')));
                        }
                    });

                    component.set('v.cityPlaces', options);
                    component.set('v.cityDropDown', true);
                } else if (response.getState() === 'ERROR') {
                    console.error('ERROR');
                }
            }));

            $A.enqueueAction(action);
        }
    },

    setAddressPlaces: function (component, key) {

        let searchedObject = component.get('v.currentBuildingMode');
        let country = component.get('v.' + searchedObject).BillingCountry;
        let city = component.get('v.' + searchedObject).BillingCity;

        key += ', ' + city + ', ' + country;

        if (key.length > 2) {
            let action = component.get('c.autocompleteRequest');

            action.setParams({
                searchKey: key,
                type: 'address'
            });

            action.setCallback(this, $A.getCallback(response => {
                if (response.getState() === 'SUCCESS') {
                    console.log('SUCCESS');

                    console.log(JSON.parse(response.getReturnValue()));

                    let predictions = JSON.parse(response.getReturnValue());
                    let options = [];

                    predictions.predictions.forEach(item => {
                        options.push(item.description);
                    });

                    component.set('v.addressPlaces', options);
                    component.set('v.addressDropDown', true);
                } else if (response.getState() === 'ERROR') {
                    console.error('ERROR');
                }
            }));

            $A.enqueueAction(action);
        }
    },

    saveFloor: function (component) {
        let action = component.get('c.saveFloor');

        action.setParams({
            accountId: component.get('v.currentBuilding').id,
            floorNumber: component.get('v.newFloor.Name')
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS save floor');
                component.set('v.newFloor.Id', response.getReturnValue());

                this.saveFloorSpecifications(component);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    saveFloorSpecifications: function (component) {

        let allSpecifications = component.get('v.floorSpecifications');


        let floorSpecifications = [];

        allSpecifications.forEach(item => {
            if (item.checked === true) {
                floorSpecifications.push({Name: item.name});
            }
        });

        console.log(floorSpecifications);

        if (floorSpecifications.length !== 0) {
            let action = component.get('c.saveSpecifications');

            action.setParams({
                specifications: floorSpecifications
            });

            action.setCallback(this, $A.getCallback(response => {
                if (response.getState() === 'SUCCESS') {
                    console.log('SUCCESS save floor\'s Specifications');
                    this.saveSpecificationToFloor(component, response.getReturnValue());
                } else if (response.getState() === 'ERROR') {
                    console.log('ERROR');
                }
            }));

            $A.enqueueAction(action);
        } else {
            component.set('v.newFloor', {});
            this.clearSpecifications(component);
            this.setBuildings(component);
        }
    },

    saveSpecificationToFloor: function (component, specificationsNamesToId) {
        let currentMode = component.get('v.currentFloorMode');
        let floorId = component.get('v.' + currentMode).Id;

        let allSpecifications = currentMode === 'newFloor'
            ? component.get('v.floorSpecifications')
            : component.get('v.editFloorSpecifications');

        let specificationsToFloor = [];
        allSpecifications.forEach(item => {
            if (item.checked === true) {
                let object = {};

                object.Floor__c = floorId;
                object.Specification__c = specificationsNamesToId[item.name];

                if (item.type === 'CheckBox') {

                    object.Type__c = item.type;
                    object.Boolean_Value__c = item.value;
                } else if (item.type === 'Number') {

                    object.Type__c = item.type;
                    object.Number_Value__c = item.value;
                } else if (item.type === 'Text') {

                    object.Type__c = item.type;
                    object.Text_Value__c = item.value;
                } else if (item.type === undefined) {

                    object.Type__c = 'CheckBox';
                    object.Boolean_Value__c = true;
                }

                specificationsToFloor.push(object);
            }
        });

        let action = component.get('c.saveSpecificationsJunction');

        action.setParams({
            specifications: specificationsToFloor
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS save Specifications to floors');

                component.set('v.' + currentMode, {});
                this.clearSpecifications(component);
                this.setBuildings(component);
                this.showSuccessToast();
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);

    },

    updateRoomOnCreateStep: function (component) {

        let newRoom = component.get('v.newRoom');

        let action = component.get('c.updateRooms');

        action.setParams({
            room: newRoom
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS save room');

                component.set('v.notAllowUpdate', true);

                this.saveRoomSpecifications(component);
                this.showSuccessToast();

            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    saveRoom: function (component) {

        let currentFloor = component.get('v.currentFloor').Id;
        let newRoom = component.get('v.newRoom');
        newRoom.Floor__c = currentFloor;
        let action = component.get('c.saveRoom');

        action.setParams({
            room: newRoom
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS save room');

                component.set('v.newRoom.Id', response.getReturnValue());

            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    saveRoomSpecifications: function (component) {

        let allSpecifications = component.get('v.roomSpecifications');

        let roomSpecifications = [];

        allSpecifications.forEach(item => {
            if (item.checked === true) {
                roomSpecifications.push({Name: item.name});
            }
        });

        if (roomSpecifications.length !== 0) {
            let action = component.get('c.saveSpecifications');

            action.setParams({
                specifications: roomSpecifications
            });

            action.setCallback(this, $A.getCallback(response => {
                if (response.getState() === 'SUCCESS') {
                    console.log('SUCCESS save room roomSpecifications');

                    this.saveSpecificationToRoom(component, response.getReturnValue());
                } else if (response.getState() === 'ERROR') {
                    console.log('ERROR');
                }
            }));

            $A.enqueueAction(action);
        } else {
            console.log('CLEAR ROOM AND SPECIFICATIONS');
            component.set('v.newRoom', {});
            this.clearSpecifications(component);
            this.setRooms(component);
        }
    },

    saveSpecificationToRoom: function (component, specificationsToId) {

        let currentMode = component.get('v.currentRoomMode');
        let roomId = component.get('v.' + currentMode).Id;

        let allSpecifications = currentMode === 'newRoom'
            ? component.get('v.roomSpecifications')
            : component.get('v.editRoomSpecifications');

        let specificationsToRoom = [];

        allSpecifications.forEach(item => {
            if (item.checked === true && specificationsToId[item.name]) {
                let object = {};

                object.Room__c = roomId;
                object.Specification__c = specificationsToId[item.name];

                if (item.type === 'CheckBox') {

                    object.Type__c = item.type;
                    object.Boolean_Value__c = item.value;
                } else if (item.type === 'Number') {

                    object.Type__c = item.type;
                    object.Number_Value__c = item.value;
                } else if (item.type === 'Text') {

                    object.Type__c = item.type;
                    object.Text_Value__c = item.value;
                } else if (item.type === undefined) {

                    object.Type__c = 'CheckBox';
                    object.Boolean_Value__c = true;
                }

                specificationsToRoom.push(object);
            }
        });

        let action = component.get('c.saveSpecificationsJunction');

        action.setParams({
            specifications: specificationsToRoom
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS save roomSpecifications to room');

                component.set('v.' + currentMode, {});
                this.clearSpecifications(component);
                this.setRooms(component);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    showSuccessToast: function () {
        /*let toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            title: 'Success',
            message: 'Record is created',
            messageTemplate: 'Record {0} created! See it {1}!',
            duration: '3000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();*/
        console.log('SUCCEESSS');
    },

    setExistBuilding: function (component, buildingId) {
        let action = component.get('c.getExistBuilding');

        action.setParams({
            buildingId: buildingId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS get exit building');
                component.set('v.editBuilding', response.getReturnValue());
                console.log(response.getReturnValue());
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    setExistBuildingSpecifications: function (component, buildingId) {
        let action = component.get('c.getExistBuildingSpecifications');

        action.setParams({
            buildingId: buildingId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS get exit building roomSpecifications');

                let options = [];
                //options = component.get('v.buildingSpecifications');

                response.getReturnValue().forEach(item => {

                    let object = {};

                    object.id = item.Id;
                    object.name = item.Specification__r.Name;
                    object.checked = true;

                    if (item.Type__c === 'CheckBox') {

                        object.type = item.Type__c;
                        object.value = item.Boolean_Value__c;
                    } else if (item.Type__c === 'Number') {

                        object.type = item.Type__c;
                        object.value = item.Number_Value__c;
                    } else if (item.Type__c === 'Text') {

                        object.type = item.Type__c;
                        object.value = item.Text_Value__c;
                    }

                    options.push(object);
                });


                component.set('v.editBuildingSpecifications', options);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    deleteBuilding: function (component, buildingId) {
        let action = component.get('c.deleteBuildings');

        action.setParams({
            buildingId: buildingId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS delete building');

                this.setBuildings(component);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    updateBuilding: function (component) {
        let editBuilding = component.get('v.editBuilding');
        let building = {};

        building.Id = editBuilding.Id;
        building.Name = editBuilding.Name;
        building.BillingCountry = editBuilding.BillingCountry;
        building.BillingCity = editBuilding.BillingCity;
        building.BillingStreet = editBuilding.BillingStreet;

        let action = component.get('c.updateBuildings');

        action.setParams({
            building: building
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS update building');

                this.updateBuildingSpecifications(component);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    updateBuildingSpecifications: function (component) {
        let buildingId = component.get('v.editBuilding').Id;
        console.log(buildingId);

        let changedSpecificationsToUpdate = [];
        let changedSpecificationsToDelete = [];
        let specificationsWithOutId = [];

        component.get('v.editBuildingSpecifications').forEach(item => {
            if (item.isChanged === true) {
                console.log(item);

                let object = {};

                object.Id = item.id;

                if (item.type === 'CheckBox') {

                    object.Type__c = item.type;
                    object.Boolean_Value__c = item.value;
                } else if (item.type === undefined) {

                    object.Type__c = 'CheckBox';
                    object.Boolean_Value__c = true;
                }

                if (!item.id) {
                    let object = {};
                    object.Name = item.name;
                    specificationsWithOutId.push(object);
                }


                if (item.checked === true && item.id) {
                    changedSpecificationsToUpdate.push(object);
                } else if (item.checked === false && item.id) {
                    changedSpecificationsToDelete.push(object);
                }

            }
        });

        if (specificationsWithOutId.length !== 0) {
            this.saveEditBuildingSpecifications(component, specificationsWithOutId);
        }

        if  (changedSpecificationsToUpdate.length !== 0 || changedSpecificationsToDelete.length !== 0) {


            let action = component.get('c.updateSpecifications');

            action.setParams({
                specificationsToUpdate: changedSpecificationsToUpdate,
                specificationsToDelete: changedSpecificationsToDelete
            });

            action.setCallback(this, $A.getCallback(response => {
                if (response.getState() === 'SUCCESS') {
                    console.log('SUCCESS update BuildingSpecifications');

                } else if (response.getState() === 'ERROR') {
                    console.log('ERROR');
                }
            }));

            $A.enqueueAction(action);
        } else if (specificationsWithOutId.length === 0) {
            this.clearSpecifications(component);
            component.set('v.newBuilding', {});
            this.setBuildings(component);
        }

    },

    saveEditBuildingSpecifications: function (component, specificationsWithOutId) {
        let action = component.get('c.saveSpecifications');

        action.setParams({
            specifications: specificationsWithOutId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS save new specification');
                console.log(response.getReturnValue());

                this.saveSpecificationToBuilding(component, response.getReturnValue());
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);

    },

    addSpecification: function (component, objectSpecifications) {

        let roomSpecifications = component.get('v.' + objectSpecifications);
        let options = [];
        let specification = {};
        let type = component.get('v.selectedSpecificationType');

        let isSpecificationExist = false;

        specification.name = component.get('v.newSpecification');
        specification.type = type;
        specification.checked = true;
        specification.isChanged = true;

        if (type === 'CheckBox') {
            specification.value = component.get('v.newSpecificationBooleanValue');
        } else if (type === 'Number') {
            specification.value = component.get('v.newSpecificationNumberValue');
        } else if (type === 'Text') {
            specification.value = component.get('v.newSpecificationTextValue');
        }
        console.log(specification.name);
        console.log(specification.value);

        roomSpecifications.forEach(item => {
            console.log(item);
            if (item.name === specification.name) {
                isSpecificationExist = true;
            }

            options.push(item);
        });

        console.log(isSpecificationExist);

        if (isSpecificationExist) {
            console.log('This specification exists')

        } else {
            options.push(specification);
        }

        component.set('v.' + objectSpecifications, options);
    },

    setExistFloor: function (component, floorId) {
        let action = component.get('c.getExistFloor');

        action.setParams({
            floorId: floorId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS get exit building');
                component.set('v.editFloor', response.getReturnValue());
                console.log(response.getReturnValue());
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    setExistFloorSpecifications: function (component, floorId) {
        let action = component.get('c.getExistFloorSpecifications');

        action.setParams({
            floorId: floorId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS get exit building roomSpecifications');

                let options = [];
                //options = component.get('v.buildingSpecifications');

                response.getReturnValue().forEach(item => {

                    let object = {};

                    object.id = item.Id;
                    object.name = item.Specification__r.Name;
                    object.checked = true;

                    if (item.Type__c === 'CheckBox') {

                        object.type = item.Type__c;
                        object.value = item.Boolean_Value__c;
                    } else if (item.Type__c === 'Number') {

                        object.type = item.Type__c;
                        object.value = item.Number_Value__c;
                    } else if (item.Type__c === 'Text') {

                        object.type = item.Type__c;
                        object.value = item.Text_Value__c;
                    }

                    options.push(object);
                });


                component.set('v.editFloorSpecifications', options);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    deleteFloor: function (component, floorId) {
        let action = component.get('c.deleteFloors');
        console.log('ID Floor: ' + floorId);
        action.setParams({
            floorId: floorId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS delete floor');

            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    updateFloor: function (component) {
        let editFloor = component.get('v.editFloor');
        let floor = {};

        console.log(editFloor);

        floor.Id = editFloor.Id;
        floor.Name = editFloor.Name;

        let action = component.get('c.updateFloors');

        action.setParams({
            floor: floor
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS update floor');

                this.updateFloorSpecifications(component);
                //this.setBuildings(component);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    updateFloorSpecifications: function (component) {
        let editFloor = component.get('v.editFloor').Id;
        console.log(editFloor);

        let changedSpecificationsToUpdate = [];
        let changedSpecificationsToDelete = [];
        let specificationsWithOutId = [];

        component.get('v.editFloorSpecifications').forEach(item => {
            if (item.isChanged === true) {
                console.log(item);

                let object = {};

                object.Id = item.id;

                if (item.type === 'CheckBox') {

                    object.Type__c = item.type;
                    object.Boolean_Value__c = item.value;
                } else if (item.type === undefined) {

                    object.Type__c = 'CheckBox';
                    object.Boolean_Value__c = true;
                }

                if (!item.id) {
                    let object = {};
                    object.Name = item.name;
                    specificationsWithOutId.push(object);
                }

                if (item.checked === true && item.id) {
                    changedSpecificationsToUpdate.push(object);
                } else if (item.checked === false && item.id) {
                    changedSpecificationsToDelete.push(object);
                }

            }
        });

        let isNull = false;

        if (specificationsWithOutId.length !== 0) {
            this.saveEditFloorSpecifications(component, specificationsWithOutId);
        } else {
            isNull = true;
        }

        if  (changedSpecificationsToUpdate.length !== 0 || changedSpecificationsToDelete.length !== 0) {

            let action = component.get('c.updateSpecifications');

            action.setParams({
                specificationsToUpdate: changedSpecificationsToUpdate,
                specificationsToDelete: changedSpecificationsToDelete
            });

            action.setCallback(this, $A.getCallback(response => {
                if (response.getState() === 'SUCCESS') {
                    console.log('SUCCESS update roomSpecifications');

                } else if (response.getState() === 'ERROR') {
                    console.log('ERROR');
                }
            }));

            $A.enqueueAction(action);

        } else if (isNull) {
            component.set('v.editFloor', {});
            this.clearSpecifications(component);
            this.setBuildings(component);
        }
    },

    saveEditFloorSpecifications: function (component, specificationsWithOutId) {

        let action = component.get('c.saveSpecifications');

        action.setParams({
            specifications: specificationsWithOutId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS save new specification');

                this.saveSpecificationToFloor(component, response.getReturnValue());
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);

    },

    setExistRoom: function (component, roomId) {
        let action = component.get('c.getExistRoom');

        action.setParams({
            roomId: roomId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS get exit room');
                console.log(response.getReturnValue());

                component.set('v.editRoom', response.getReturnValue());
                this.setContent(component);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    setContent: function (component) {
        let roomMode = component.get('v.currentRoomMode');

        let roomId = component.get('v.' + roomMode).Id;
        console.log(component.get('v.editRoom'));

        let action = component.get('c.getRoomPictures');

        action.setParams({
            roomId: roomId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS');

                let options = [];

                response.getReturnValue().forEach(item => {
                    options.push({
                        documentId: item.ContentDocumentId,

                        name: item.Title.length > 15 ? item.Title.substring(0, 15) + '...' : item.Title,
                        id: item.Id,
                        description: item.Description
                    });
                });
                console.log(options);
                component.set('v.uploadedFiles', options);
                console.log(response.getReturnValue());
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    setExistRoomSpecifications: function (component, roomId) {
        let action = component.get('c.getExistRoomSpecifications');

        action.setParams({
            roomId: roomId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS get exit room roomSpecifications');

                let options = [];

                response.getReturnValue().forEach(item => {

                    let object = {};

                    object.id = item.Id;
                    object.name = item.Specification__r.Name;
                    object.checked = true;

                    if (item.Type__c === 'CheckBox') {

                        object.type = item.Type__c;
                        object.value = item.Boolean_Value__c;
                    } else if (item.Type__c === 'Number') {

                        object.type = item.Type__c;
                        object.value = item.Number_Value__c;
                    } else if (item.Type__c === 'Text') {

                        object.type = item.Type__c;
                        object.value = item.Text_Value__c;
                    }

                    options.push(object);
                });


                component.set('v.editRoomSpecifications', options);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    deleteRoom: function (component, roomId) {
        let action = component.get('c.deleteRooms');

        action.setParams({
            roomId: roomId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS delete room');

                this.setRooms(component);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    updateRoom: function (component) {
        let editRoom = component.get('v.editRoom');

        let action = component.get('c.updateRooms');

        action.setParams({
            room: editRoom
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS update floor');

                this.updateRoomSpecifications(component);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    updateRoomSpecifications: function (component) {
        let editRoom = component.get('v.editRoom').Id;
        console.log(editRoom);

        let changedSpecificationsToUpdate = [];
        let changedSpecificationsToDelete = [];
        let specificationsWithOutId = [];

        component.get('v.editRoomSpecifications').forEach(item => {
            if (item.isChanged === true) {
                console.log(item);

                let object = {};

                object.Id = item.id;

                if (item.type === 'CheckBox') {

                    object.Type__c = item.type;
                    object.Boolean_Value__c = item.value;
                } else if (item.type === undefined) {

                    object.Type__c = 'CheckBox';
                    object.Boolean_Value__c = true;
                }

                if (!item.id) {
                    let object = {};
                    object.Name = item.name;
                    specificationsWithOutId.push(object);
                }

                if (item.checked === true && item.id) {
                    changedSpecificationsToUpdate.push(object);
                } else if (item.checked === false && item.id) {
                    changedSpecificationsToDelete.push(object);
                }

            }
        });

        let isNull = false;

        if (specificationsWithOutId.length !== 0) {
            this.saveEditRoomSpecifications(component, specificationsWithOutId);
        } else {
            isNull = true;
        }

        if  (changedSpecificationsToUpdate.length !== 0 || changedSpecificationsToDelete.length !== 0) {

            let action = component.get('c.updateSpecifications');

            action.setParams({
                specificationsToUpdate: changedSpecificationsToUpdate,
                specificationsToDelete: changedSpecificationsToDelete
            });

            action.setCallback(this, $A.getCallback(response => {
                if (response.getState() === 'SUCCESS') {
                    console.log('SUCCESS update roomSpecifications');

                } else if (response.getState() === 'ERROR') {
                    console.log('ERROR');
                }
            }));

            $A.enqueueAction(action);
        } else if (isNull) {
            component.set('v.editRoom', {});
            this.clearSpecifications(component);
        }

    },

    saveEditRoomSpecifications: function (component, specificationsWithOutId) {
        let action = component.get('c.saveSpecifications');

        action.setParams({
            specifications: specificationsWithOutId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS save new specification');
                console.log(response.getReturnValue());

                this.saveSpecificationToRoom(component, response.getReturnValue());
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    deletePicture: function (component) {
        let picId = component.get('v.selectedPictureDocumentId');

        let action = component.get('c.deletePicture');

        action.setParams({
            pictureId: picId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS delete picture');

                this.setContent(component);
                component.set('v.sureToDeletePic', false);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    saveMainPicture: function (component) {
        let picId = component.get('v.selectedPictureDocumentId');
        let roomMode = component.get('v.currentRoomMode');
        let roomId = component.get('v.' + roomMode).Id;

        let action = component.get('c.setMainPictures');

        action.setParams({
            roomId: roomId,
            picId: picId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS save Main picture');
                component.set('v.sureToDeletePic', false);

                this.setContent(component);
                this.setRooms(component);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    clearSpecifications: function (component) {
        component.set('v.newSpecification', '');
        component.set('v.newSpecificationBooleanValue', null);
        component.set('v.newSpecificationNumberValue', '');
        component.set('v.newSpecificationTextValue', '');
    },

    getNonApprovedObjects: function (component) {
        let action = component.get('c.getNonApprovedObjects');

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS get Non Approved Objects');

                let buildings = [];
                let floors = [];
                let rooms = [];
                let roomsId = [];

                let objects = JSON.parse(response.getReturnValue());

                console.log(objects);

                objects.buildings.forEach(item => {
                    let object = {};

                    object.name = item.Name;

                    if (item.BillingCountry && item.BillingCity && item.BillingStreet) {

                        object.billingAddress = item.BillingStreet + ', ' + item.BillingCity + ', ' + item.BillingCountry;
                    } else if (item.BillingCountry && item.BillingCity) {

                        object.billingAddress = item.BillingCity + ', ' + item.BillingCountry
                    } else if (item.BillingCountry) {

                        object.billingAddress = item.BillingCountry;
                    } else {
                        object.billingAddress = ' Not defined';
                    }

                    object.status = item.Status__c;

                    buildings.push(object);
                });

                objects.floors.forEach(item => {
                    let object = {};

                    object.name = item.Name;
                    object.status = item.Status__c;
                    object.buildingName = item.Account__r.Name;

                    floors.push(object);
                });

                objects.rooms.forEach(item => {
                    let object = {};

                    object.id = item.Id;
                    object.name = item.Name;
                    object.status = item.Status__c;
                    object.type = item.Type__c;
                    object.price = item.Price__c;
                    object.isHourlyPayment = item.Hourly_Wage__c;
                    object.description = item.Description__c;
                    object.floorName = item.Floor__r.Name;
                    object.buildingName = item.Floor__r.Account__r.Name;

                    rooms.push(object);
                    roomsId.push(item.Id);
                });

                let object = {
                    buildings: buildings,
                    floors: floors,
                    rooms: rooms
                };

                this.setMainPicturesForNonApprovedRooms(component, roomsId);

                component.set('v.nonApprovedObjects', object);
                console.log(buildings);
                console.log(floors);
                console.log(rooms);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    setPicture: function (component) {
        let action = component.get('c.getPicture');

        action.setParams({
            picId: component.get('v.selectedPictureDocumentId')
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS set picture');

                component.set('v.pictureUrl', response.getReturnValue());
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    setMainPicturesForNonApprovedRooms: function (component, roomsId) {
        let action = component.get('c.getMainPictures');

        action.setParams({
            roomsId: roomsId
        });

        action.setCallback(this, $A.getCallback(response => {
            if (response.getState() === 'SUCCESS') {
                console.log('SUCCESS get rooms pictures');
                console.log(JSON.parse(response.getReturnValue()));

                let wrappers = JSON.parse(response.getReturnValue());
                let rooms = component.get('v.nonApprovedObjects.rooms');

                rooms.forEach(room => {
                    wrappers.forEach(item => {
                        if (room.id === item.roomId) {
                            room.picUrl = item.url;
                        }
                    })
                });

                component.set('v.nonApprovedObjects.rooms', rooms);
            } else if (response.getState() === 'ERROR') {
                console.log('ERROR');
            }
        }));

        $A.enqueueAction(action);
    },

    checkContent: function (component) {
        if (component.get('v.uploadedFiles').length !== 0) {
            let action = component.get('c.setEditedStatusToRoom');

            action.setParams({
                roomId: component.get('v.editRoom').Id
            });

            action.setCallback(this, $A.getCallback(response => {
                if (response.getState() === 'SUCCESS') {
                    console.log('SUCCESS set status edited to room');

                } else if (response.getState() === 'ERROR') {
                    console.log('ERROR');
                }
            }));

            $A.enqueueAction(action);
        }
    }

});