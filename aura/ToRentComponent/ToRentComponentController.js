({

    doInit: function (component, event, helper) {

        component.set('v.newBuilding', {});
        component.set('v.newRoom', {});
        component.set('v.newFloor', {});

        helper.changeCSSpecifications(component);
        helper.setBuildingSpecifications(component);
        helper.setFloorSpecifications(component);
        helper.setBuildings(component);


        component.set('v.roomTypes', [
            'Room',
            'Garage',
            'Something Else'
        ]);

        component.set('v.specificationTypes', [
            'CheckBox',
            'Number',
            'Text'
        ]);

        component.set('v.countryDropDown', false);
        component.set('v.cityDropDown', false);
        component.set('v.addressDropDown', false);
    },

    jsLoaded: function(cmp, event, helper) {

    },

    changeStep: function (component, event, helper) {
        let currentStep = event.getSource().get('v.value');

        if (currentStep === 'step4') {
            helper.getNonApprovedObjects(component);
        }

        component.set('v.currentStep', currentStep);
    },

    handleUploadFinished: function (component, event, helper) {

        helper.setContent(component);
        helper.checkContent(component);

        console.log('upload is finished');
    },

    saveBuilding: function (component, event, helper) {
        helper.saveBuilding(component);
        component.set('v.createModeBuilding', false);
    },

    saveFloorAndSpecifications: function (component, event, helper) {
        helper.saveFloor(component);
        component.set('v.createModeFloor', false);
    },


    handleRoomTypeChange: function (component, event, helper) {

        helper.changeCSSpecifications(component);
    },

    addRoomSpecification: function (component, event, helper) {

        helper.addSpecification(component, 'roomSpecifications');
    },

    addEditRoomSpecification: function (component, event, helper) {

        helper.addSpecification(component, 'editRoomSpecifications');
    },

    addBuildingSpecification: function (component, event, helper) {

        helper.addSpecification(component, 'buildingSpecifications');
    },

    addEditBuildingSpecification: function (component, event, helper) {

        helper.addSpecification(component, 'editBuildingSpecifications');
    },

    addFloorSpecification: function (component, event, helper) {

        helper.addSpecification(component, 'floorSpecifications');
    },

    addEditFloorSpecification: function (component, event, helper) {

        helper.addSpecification(component, 'editFloorSpecifications');
    },

    handleCountryInput: function (component, event, helper) {

        let searchedObject = component.get('v.currentBuildingMode');
        let key = component.get('v.' + searchedObject).BillingCountry;

        clearTimeout(component.get('v.timerId'));

        if (key.length > 2) {
            component.set('v.timerId', window.setTimeout(
                $A.getCallback(function () {
                    helper.setCountryPlaces(component, key)
                }), 500
            ));
        } else {
            component.set('v.countryPlaces', []);
        }
    },

    handleCityInput: function (component, event, helper) {
        let searchedObject = component.get('v.currentBuildingMode');
        let key = component.get('v.' + searchedObject).BillingCity;

        clearTimeout(component.get('v.timerId'));

        if (key.length > 2) {
            component.set('v.timerId', window.setTimeout(
                $A.getCallback(function () {
                    helper.setCityPlaces(component, key)
                }), 500
            ));
        } else {
            component.set('v.cityPlaces', []);
        }


    },

    handleAddressInput: function (component, event, helper) {
        let searchedObject = component.get('v.currentBuildingMode');
        let key = component.get('v.' + searchedObject).BillingStreet;

        clearTimeout(component.get('v.timerId'));

        if (key.length > 2) {
            component.set('v.timerId', window.setTimeout(
                $A.getCallback(function () {
                    helper.setAddressPlaces(component, key)
                }), 500
            ));
        } else {
            component.set('v.addressPlaces', []);
        }


    },

    saveRoomAndSpecifications: function (component, event, helper) {

        helper.updateRoomOnCreateStep(component);
        component.set('v.createModeRoom', false);
    },

    onblurSaveRoom: function (component, event, helper) {


        helper.saveRoom(component);
        component.set('v.notAllowUpdate', false);
    },

    selectCountry: function (component, event, helper) {
        let country = event.target.innerText;
        let searchedObject = component.get('v.currentBuildingMode');

        component.set('v.' + searchedObject + '.BillingCountry', country);
        component.set('v.countryDropDown', false);
    },

    selectCity: function (component, event, helper) {
        let city = event.target.innerText;
        let searchedObject = component.get('v.currentBuildingMode');

        component.set('v.' + searchedObject + '.BillingCity', city);
        component.set('v.cityDropDown', false);
    },

    selectAddress: function (component, event, helper) {
        let address = event.target.innerText;
        let searchedObject = component.get('v.currentBuildingMode');

        address = address.substring(0, address.indexOf(','));

        component.set('v.' + searchedObject + '.BillingStreet', address);
        component.set('v.addressDropDown', false);
    },

    focusCountryInput: function (component, event, helper) {
        component.set('v.countryDropDown', true);
    },

    focusCityInput: function (component, event, helper) {
        component.set('v.cityDropDown', true);
    },

    focusAddressInput: function (component, event, helper) {
        component.set('v.addressDropDown', true);
    },

    blurCountryInput: function (component, event, helper) {
        component.set('v.countryDropDown', false);

    },

    handleSectionToggle: function (component, event) {
        let openSections = event.getParam('openSections');
        console.log(openSections);

        component.set('v.activeSections', openSections)
    },

    showPopUpBuilding: function (component) {
        component.set('v.createModeBuilding', true);
        component.set('v.currentBuildingMode', 'newBuilding');
    },

    hidePopUpBuilding: function (component) {
        component.set('v.createModeBuilding', false);
    },

    showPopUpFloor: function (component) {
        component.set('v.createModeFloor', true);
        component.set('v.currentFloorMode', 'newFloor');
    },

    hidePopUpFloor: function (component) {
        component.set('v.createModeFloor', false);
    },

    showPopUpRoom: function (component) {
        component.set('v.createModeRoom', true);
        component.set('v.currentRoomMode', 'newRoom');
        component.set('v.uploadedFiles', {});
    },

    hidePopUpRoom: function (component) {
        component.set('v.createModeRoom', false);
    },

    showPopUpBuildingEdit: function (component) {
        component.set('v.editModeBuilding', true);
    },

    hidePopUpBuildingEdit: function (component) {
        component.set('v.editModeBuilding', false);
    },

    showPopUpFloorEdit: function (component) {
        component.set('v.editModeFloor', true);
    },

    hidePopUpFloorEdit: function (component) {
        component.set('v.editModeFloor', false);
    },

    showPopUpRoomEdit: function (component) {
        component.set('v.editModeRoom', true);
    },

    hidePopUpRoomEdit: function (component) {
        component.set('v.editModeRoom', false);
    },

    selectBuilding: function (component, event, helper) {
        let selectedBuilding = event.target.value;
        console.log(component.get('v.buildings')[selectedBuilding]);
        component.set('v.currentBuildingNumber', selectedBuilding);
        component.set('v.currentBuilding', component.get('v.buildings')[selectedBuilding]);

        $A.util.removeClass(component.find('step2'), 'step2');
    },

    selectFloor: function (component, event, helper) {
        let floorNumber = event.target.value;
        let currentBuilding = component.get('v.currentBuilding');

        component.set('v.currentFloor', currentBuilding.floors[floorNumber]);
        $A.util.removeClass(component.find('step3'), 'step3');

        helper.setRooms(component);
    },

    editBuilding: function (component, event, helper) {
        let selectedBuilding = event.getSource().get('v.value');
        let buildingId = component.get('v.buildings')[selectedBuilding].id;

        component.set('v.editModeBuilding', true);
        component.set('v.currentBuildingMode', 'editBuilding');

        helper.setExistBuilding(component, buildingId);
        helper.setExistBuildingSpecifications(component, buildingId);
    },

    deleteBuilding: function (component, event, helper) {
        let selectedBuilding = event.getSource().get('v.value');
        let buildingId = component.get('v.buildings')[selectedBuilding].id;

        helper.deleteBuilding(component, buildingId);
    },

    updateBuilding: function (component, event, helper) {
        helper.updateBuilding(component);
        component.set('v.editModeBuilding', false);
    },

    changeBuildingSpecification: function (component, event) {
        console.log(event.getSource().get('v.id'));

        let specificationNumber = event.getSource().get('v.id');
        let roomSpecifications = component.get('v.editBuildingSpecifications');

        roomSpecifications[specificationNumber].isChanged = true;
        component.set('v.editBuildingSpecifications', roomSpecifications);
    },

    changeFloorSpecification: function (component, event) {
        console.log(event.getSource().get('v.id'));

        let specificationNumber = event.getSource().get('v.id');
        let roomSpecifications = component.get('v.editFloorSpecifications');

        roomSpecifications[specificationNumber].isChanged = true;
        component.set('v.editBuildingSpecifications', roomSpecifications);
    },

    changeRoomSpecification: function (component, event) {
        console.log(event.getSource().get('v.id'));

        let specificationNumber = event.getSource().get('v.id');
        let roomSpecifications = component.get('v.editRoomSpecifications');

        roomSpecifications[specificationNumber].isChanged = true;
        component.set('v.editRoomSpecifications', roomSpecifications);
    },

    editFloor: function (component, event, helper) {
        let floorNumber = event.getSource().get('v.value');
        let currentBuilding = component.get('v.currentBuilding');

        component.set('v.currentFloor', currentBuilding.floors[floorNumber]);
        component.set('v.currentFloorMode', 'editFloor');
        component.set('v.editModeFloor', true);

        helper.setExistFloor(component, component.get('v.currentFloor').Id);
        helper.setExistFloorSpecifications(component, component.get('v.currentFloor').Id);
    },

    deleteFloor: function (component, event, helper) {
        let floorNumber = event.getSource().get('v.value');
        let currentBuilding = component.get('v.currentBuilding');

        console.log(floorNumber);
        console.log(currentBuilding);

        helper.deleteFloor(component, currentBuilding.floors[floorNumber].Id);
        helper.setBuildings(component);
    },

    updateFloor: function (component, event, helper) {
        helper.updateFloor(component);
        component.set('v.editModeFloor', false);
    },

    editRoom: function (component, event, helper) {
        let roomNumber = event.getSource().get('v.value');
        let rooms = component.get('v.rooms');

        component.set('v.editModeRoom', true);
        component.set('v.currentRoomMode', 'editRoom');

        helper.setExistRoom(component, rooms[roomNumber].Id);
        helper.setExistRoomSpecifications(component, rooms[roomNumber].Id);
    },

    deleteRoom: function (component, event, helper) {
        let roomNumber = event.getSource().get('v.value');
        let rooms = component.get('v.rooms');

        helper.deleteRoom(component, rooms[roomNumber].Id);
    },

    updateRoom: function (component, event, helper) {
        helper.updateRoom(component);

        helper.setRooms(component);

        component.set('v.editModeRoom', false);
    },

    showDeletePopUp: function (component, event, helper) {
        component.set('v.sureToDeletePic', true);

        let picIndex = event.getSource().get('v.id');
        let pictures = component.get('v.uploadedFiles');

        component.set('v.selectedPictureDocumentId', pictures[picIndex].documentId);
        component.set('v.selectedPictureId', pictures[picIndex].id);

        helper.setPicture(component);
    },

    hideDeletePopUp: function (component) {
        component.set('v.sureToDeletePic', false);
    },

    deleteCurrentPicture: function (component, event, helper) {
        helper.deletePicture(component);
    },

    setMainPicture: function (component, event, helper) {
        helper.saveMainPicture(component);
    },

    showSpecifications: function (component, event) {
        let menu = document.getElementById("dropdown-menu");

        menu.style.display === 'none' ? $("#dropdown-menu").slideDown() : $("#dropdown-menu").slideUp();
    }

});