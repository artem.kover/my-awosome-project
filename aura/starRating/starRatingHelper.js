({
    renderStars: function(cmp) {
        let rating = Number(cmp.get('v.value'));
        cmp.set('v.fixedValue', rating.toFixed(2));
        let starsRender = '';
        let color = cmp.get('v.color');

        for (let i = 1; i < 6; i++) {
            if (rating >= i) {
                starsRender += `<i class="fas fa-star" id="full" style="color: ${color};" ></i>`;
            }
            if (rating > (i - 1) && rating < i) {
                starsRender += `<i class="fas fa-star" style="color: grey; position: relative;">
                                    <i class="fas fa-star-half" id="half" style="color: ${color}; position: absolute; left: 0;top: 0;"></i>
                                </i>`;
            } else if (rating < i) {
                starsRender += `<i class="fas fa-star" id="half" style="color: grey;" ></i>`;
            }
        }
        // let element = document.getElementById(`starsRender${cmp.get('v.number')}`);
        // if (element) {
        //     element.innerHTML = starsRender;
        //     element.style.fontSize = cmp.get('v.size');
        // }
        cmp.set('v.starsBody', starsRender);
        // console.log('assdgadyhdtfshdgfshdfshs');
    }
});