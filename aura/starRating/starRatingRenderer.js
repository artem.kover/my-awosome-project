({
    afterRender: function (cmp, helper) {
        this.superAfterRender();
        helper.renderStars(cmp);
    },

    rerender : function(cmp, helper) {
        this.superRerender();
        helper.renderStars(cmp);
    }

});