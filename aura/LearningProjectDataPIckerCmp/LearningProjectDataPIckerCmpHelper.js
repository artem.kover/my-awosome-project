({
    initHelper : function(component, event, helper) {

        var action = component.get('c.isHourlyWage'); 
        action.setParams({id : component.get('v.roomId')});
        action.setCallback(this,$A.getCallback(function (response){
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log('is valid', response.getReturnValue());
                helper.initAfterMain(component, event, helper, response.getReturnValue());
            }
            else if (state === 'ERROR') console.error(response.getError());
        }));

        $A.enqueueAction(action);

    },

    initAfterMain : function(component, event, helper, isValid) {

        var action = component.get('c.getBookingDate'); 
        action.setParams({id : component.get('v.roomId')});
        action.setCallback(this,$A.getCallback(function (response){
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.hourlyWage', response.getReturnValue());

                let data = JSON.parse(response.getReturnValue());
                component.set('v.data', JSON.parse(response.getReturnValue()));

                if(isValid == true) {
                    let datePicker = component.find("datePickerContainer");
                    $A.util.removeClass(datePicker, 'hideElement');

                    let arr = [];
        
                    for(let i = 0; i < 24; i++) {
                        arr.push([
                            {'label': '00', 'value': i + ':00', valid: true},
                            {'label': '15', 'value': i + ':15', valid: true},
                            {'label': '30', 'value': i + ':30', valid: true},
                            {'label': '45', 'value': i + ':45', valid: true}
                        ]);
                    }

                    component.set('v.options', arr);
                } 
                else {
                    let datePicker = component.find('datePickerWithOutTime');
                    $A.util.removeClass(datePicker, 'hideElement');
                    let disabledDates = [];

                    for(let i = 0; i < data.length; i++){
                        let startDate = new Date(data[i].Start_Date_Time__c);
                        let endDate = new Date(data[i].End_Date_Time__c);
                        
                        if(new Date() > endDate) continue;

                        disabledDates.push(helper.getDateBetweenDates(startDate, endDate, helper));
                    }

                    component.set('v.disabledDates', helper.deleteDuplicates([].concat(...disabledDates)));
                    console.log(helper.deleteDuplicates([].concat(...disabledDates)));

                }
            }
            else if (state === 'ERROR') console.error(response.getError());
        }));

        $A.enqueueAction(action);

    },

    deleteDuplicates : function(arr) {
        return arr.filter((item, pos) => arr.indexOf(item) == pos)
    },

    getDateBetweenDates : function(dateStart, dateEnd, helper) {
        let massDates = [];
        massDates.push(`${dateStart.getDate()} ${dateStart.getMonth()+1} ${dateStart.getFullYear()}`);
        let daysBetweenDates = helper.diffDates(dateEnd, dateStart);
        for(let i = 0; i < daysBetweenDates; i++) {
            dateStart.setDate(dateStart.getDate() + 1);
            massDates.push(`${dateStart.getDate()} ${dateStart.getMonth()+1} ${dateStart.getFullYear()}`);
        }
        return massDates;
    },

    diffDates : function(day_one, day_two) {
        return (day_one - day_two) / (60 * 60 * 24 * 1000);
    },

    getDateWithTime : function(date, hour, minute) {
        hour = hour < 10 ? `0${hour}` : hour;
        return `${date}T${hour}:${minute}:00.000+0000`;
    },

    checkValidTime : function(data, currentTimeStart, currentTimeEnd) {
        for(let i = 0; i < data.length; i++) {
            let startTime = data[i].Start_Date_Time__c;
            let endTime = data[i].End_Date_Time__c;
            if(!(currentTimeStart < startTime && currentTimeStart < startTime && endTime > currentTimeEnd && endTime > currentTimeEnd ||currentTimeStart > startTime && currentTimeStart > startTime && endTime < currentTimeEnd && endTime < currentTimeEnd)) {
                return false;
            }
        }
        return true;
    },

    getTimeDateHelper : function(component, event, helper) {
        let data = component.get('v.data');
        console.log('before');
        let date = event.currentTarget.value;
        let arr = [];
        console.log(date);
        for(let i = 0; i < 24; i++) {
            let hour = i < 10 ? `0${i}` : i;
            arr.push([
                {'label': '00', 'value': `${hour}:00-${hour}:15`, valid: helper.checkValidTime(data, helper.getDateWithTime(date, i, '00'), helper.getDateWithTime(date, i, '15'))},
                {'label': '15', 'value': `${hour}:15-${hour}:30`, valid: helper.checkValidTime(data, helper.getDateWithTime(date, i, '15'), helper.getDateWithTime(date, i, '30'))},
                {'label': '30', 'value': `${hour}:30-${hour}:45`, valid: helper.checkValidTime(data, helper.getDateWithTime(date, i, '30'), helper.getDateWithTime(date, i, '45'))},
                {'label': '45', 'value': `${hour}:45-${+hour+1}:00`, valid: helper.checkValidTime(data, helper.getDateWithTime(date, i, '45'), helper.getDateWithTime(date, i + 1, '00'))}
            ]);
        }
        console.log(arr);
        component.set('v.options', arr);
        component.set('v.bookingTime',[]);
        console.log(arr);
    },

    changeTimeHelper : function(component, event, helper) {
        let elements = component.find('tableItem');
        if(event.currentTarget.dataset.action === 'true') {
            event.currentTarget.dataset.action = 'false';
            let row = event.currentTarget.dataset.rowindex;
            let item = event.currentTarget.dataset.elementindex;
            $A.util.removeClass(elements[row*4 + +item], "green");
            // console.log(row);
            // row = (row < 10) ? `0${row}` : row;
            component.set('v.bookingTime', helper.removeBookingTime(component.get('v.bookingTime'),component.get('v.options')[row][item].value));
        } else {
            event.currentTarget.dataset.action = 'true';
            let row = event.currentTarget.dataset.rowindex;
            let item = event.currentTarget.dataset.elementindex;
            $A.util.addClass(elements[row*4 + +item], "green");
            let bookingTime = component.get('v.bookingTime');
            bookingTime.push(component.get('v.options')[row][item].value);
            component.set('v.bookingTime', bookingTime);
        }

        console.log(event.currentTarget.dataset.action);

    }, 

    // changeAllTimeHelper : function(component, event, helper) {
    //     let elements = component.find('tableItem');
    //     let row = event.currentTarget.dataset.active;
    //     console.log(row);
    //     if(event.currentTarget.dataset.active === 'true') {
    //         // elements[row*4 + +0].dataset.active = 'false';
    //         // elements[row*4 + +1].dataset.active = 'false';
    //         // elements[row*4 + +2].dataset.active = 'false';
    //         // elements[row*4 + +3].dataset.active = 'false';
    //         let item = event.currentTarget.dataset.elementindex;
    //         $A.util.removeClass(elements[row*4 + +0], "green");
    //         $A.util.removeClass(elements[row*4 + +1], "green");
    //         $A.util.addClass(elements[row*4 + +1], "black");
    //         $A.util.removeClass(elements[row*4 + +2], "green");
    //         $A.util.addClass(elements[row*4 + +2], "black");
    //         $A.util.removeClass(elements[row*4 + +3], "green");
    //         $A.util.addClass(elements[row*4 + +3], "black");
    //         // console.log(row);
    //         // row = (row < 10) ? `0${row}` : row;
    //         component.set('v.bookingTime', helper.removeBookingTime(component.get('v.bookingTime'),component.get('v.options')[row][0].value));
    //         component.set('v.bookingTime', helper.removeBookingTime(component.get('v.bookingTime'),component.get('v.options')[row][1].value));
    //         component.set('v.bookingTime', helper.removeBookingTime(component.get('v.bookingTime'),component.get('v.options')[row][2].value));
    //         component.set('v.bookingTime', helper.removeBookingTime(component.get('v.bookingTime'),component.get('v.options')[row][3].value));
    //     } else {
    //         // elements[row*4 + +0].dataset.active = 'true';
    //         // elements[row*4 + +1].dataset.active = 'true';
    //         // elements[row*4 + +2].dataset.active = 'true';
    //         // elements[row*4 + +3].dataset.active = 'true';
    //         let item = event.currentTarget.dataset.elementindex;
    //         $A.util.removeClass(elements[row*4 + +0], "black");
    //         $A.util.addClass(elements[row*4 + +0], "green");
    //         $A.util.removeClass(elements[row*4 + +1], "black");
    //         $A.util.addClass(elements[row*4 + +1], "green");
    //         $A.util.removeClass(elements[row*4 + +2], "black");
    //         $A.util.addClass(elements[row*4 + +2], "green");
    //         $A.util.removeClass(elements[row*4 + +3], "black");
    //         $A.util.addClass(elements[row*4 + +3], "green");
    //         let bookingTime = component.get('v.bookingTime');
    //         bookingTime.push(component.get('v.options')[row][0].value);
    //         bookingTime.push(component.get('v.options')[row][1].value);
    //         bookingTime.push(component.get('v.options')[row][2].value);
    //         bookingTime.push(component.get('v.options')[row][3].value);
    //         component.set('v.bookingTime', bookingTime);
            
    //     }
    // },

    removeBookingTime : function(BookingTimes, BookingTime) {
        for(let i = 0; i < BookingTimes.length; i++) {
            if(BookingTimes.indexOf(BookingTime) == -1) {
                break;
            }
            BookingTimes.splice(BookingTimes.indexOf(BookingTime),1);
        }
        return BookingTimes;
    },

    getCurrentDateHelper : function(component, event, helper) {
        component.set('v.currentDate', component.find('datepicker').get('v.value'));
        console.log(component.find('datepicker').get('v.value'));
    },

    afterLoader : function(component, event, helper) {
        console.log('>>>>EVENT<<<<', event);

        var xxx = {};

        $(document).ready(function() {

            let disabledDates = component.get('v.disabledDates');

            $('input.datePickerWithTime').Zebra_DatePicker({
                direction: true,
                onSelect: function(view, elements) {
                    console.log($('input.datePickerWithTime').val());
                    let data = component.get('v.data');
                    console.log(data);
                    let date = $('input.datePickerWithTime').val();
                    let arr = [];
                    console.log(date);
                    for(let i = 0; i < 24; i++) {
                        let hour = i < 10 ? `0${i}` : i;
                        arr.push([
                            {'label': '00', 'value': `${hour}:00-${hour}:15`, valid: helper.checkValidTime(data, helper.getDateWithTime(date, i, '00'), helper.getDateWithTime(date, i, '15'))},
                            {'label': '15', 'value': `${hour}:15-${hour}:30`, valid: helper.checkValidTime(data, helper.getDateWithTime(date, i, '15'), helper.getDateWithTime(date, i, '30'))},
                            {'label': '30', 'value': `${hour}:30-${hour}:45`, valid: helper.checkValidTime(data, helper.getDateWithTime(date, i, '30'), helper.getDateWithTime(date, i, '45'))},
                            {'label': '45', 'value': `${hour}:45-${+hour+1}:00`, valid: helper.checkValidTime(data, helper.getDateWithTime(date, i, '45'), helper.getDateWithTime(date, i + 1, '00'))}
                        ]);
                    }
                    component.set('v.options', arr);
                    component.set('v.bookingTime',[]);
                }
            });

            $('.datePickerStartDate').Zebra_DatePicker({
                disabled_dates: disabledDates,
                direction: true,
                pair: $('.datePickerEndDate')
            });

            $('.datePickerEndDate').Zebra_DatePicker({
                disabled_dates: disabledDates,
                direction: true
            });
            $('.Zebra_DatePicker_Icon').css("top", "3px");
            $('.Zebra_DatePicker_Icon').css("background-image", "url('https://mainlearnprojectmfk-dev-ed--c.visualforce.com/resource/1576520016000/zebra_datapicker_icons')");

            $('.dp_weekend').click(()=>{
                console.log('qweqwe');
            });

            $('.dp_weekend').attr('onClick', 'testChange(this);');

            $('.dp_weekend').bind('click', function() {
                alert('User clicked on "foo."');
            });

        });
    
    }
})