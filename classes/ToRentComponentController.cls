public with sharing class ToRentComponentController {

    @AuraEnabled
    public static List<Account> getBuildings() {
        return [
                SELECT
                        Name, BillingAddress, (
                        SELECT Name
                        FROM Floors__r
                )
                FROM Account
                WHERE OwnerId = :UserInfo.getUserId()
        ];
    }

    @AuraEnabled
    public static Account getExistBuilding(String buildingId) {
        return [SELECT Name, BillingAddress, BillingCountry, BillingCity, BillingStreet FROM Account WHERE Id = :buildingId LIMIT 1];
    }

    @AuraEnabled
    public static List<Specification_Junction__c> getExistBuildingSpecifications(String buildingId) {
        List<Specification_Junction__c> specifications = [
                SELECT
                        Account__c, Type__c, Boolean_Value__c, Number_Value__c, Text_Value__c,
                        Specification__r.Name
                FROM Specification_Junction__c
                WHERE Account__c = :buildingId
        ];

        return specifications;
    }

    @AuraEnabled
    public static Floor__c getExistFloor(String floorId) {
        return [SELECT Name FROM Floor__c WHERE Id = :floorId LIMIT 1];
    }

    @AuraEnabled
    public static List<Specification_Junction__c> getExistFloorSpecifications(String floorId) {
        List<Specification_Junction__c> specifications = [
                SELECT
                        Floor__c, Type__c, Boolean_Value__c, Number_Value__c, Text_Value__c,
                        Specification__r.Name
                FROM Specification_Junction__c
                WHERE Floor__C = :floorId
        ];

        return specifications;
    }

    @AuraEnabled
    public static Room__c getExistRoom(String roomId) {
        return [
                SELECT
                        Name, Type__c, Price__c, Hourly_Wage__c, Description__c
                FROM Room__c
                WHERE Id = :roomId
                LIMIT 1
        ];
    }

    @AuraEnabled
    public static List<Specification_Junction__c> getExistRoomSpecifications(String roomId) {
        List<Specification_Junction__c> specifications = [
                SELECT
                        Floor__c, Type__c, Boolean_Value__c, Number_Value__c, Text_Value__c,
                        Specification__r.Name
                FROM Specification_Junction__c
                WHERE Room__c = :roomId
        ];

        return specifications;
    }

    @AuraEnabled
    public static void deleteBuildings(String buildingId) {
        delete [SELECT Id FROM Account WHERE Id = :buildingId];
    }

    @AuraEnabled
    public static void updateBuildings(Account building) {
        building.Status__c = 'Edited';
        update building;
    }

    @AuraEnabled
    public static void updateSpecifications(List<Specification_Junction__c> specificationsToUpdate, List<Specification_Junction__c> specificationsToDelete) {
        update specificationsToUpdate;
        delete specificationsToDelete;
    }

    @AuraEnabled
    public static void updateFloors(Floor__c floor) {
        floor.Status__c = 'Edited';
        update floor;
    }

    @AuraEnabled
    public static void deleteFloors(String floorId) {
        delete [SELECT Id FROM Floor__c WHERE Id = :floorId];
    }

    @AuraEnabled
    public static void updateRooms(Room__c room) {
        room.Status__c = 'Edited';
        update room;
    }

    @AuraEnabled
    public static void deleteRooms(String roomId) {

        Set<Id> contentIds = new Set<Id>();


        List<ContentDocumentLink> contentDocumentLinks = [
                SELECT
                        ContentDocumentId
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :roomId
        ];

        for (ContentDocumentLink item : contentDocumentLinks) {
            contentIds.add(item.ContentDocumentId);
        }

        List<ContentDocument> content = [
                SELECT
                        Id
                FROM ContentDocument
                WHERE Id IN :contentIds
        ];

        delete [SELECT Id FROM ContentDocument WHERE Id IN :contentIds];
        delete [SELECT Id FROM Room__c WHERE Id = :roomId];
    }

    @AuraEnabled
    public static Id saveSpecification(String specificationName) {
        Specification__c specification = new Specification__c(Name = specificationName);

        insert specification;

        return specification.Id;
    }

    @AuraEnabled
    public static List<Room__c> getRooms(String floorId) {
        return [SELECT Name, Price__c, Type__c FROM Room__c WHERE Floor__c = :floorId];
    }

    @AuraEnabled
    public static List<BuildingSpecifications__c> getBuildingSpecifications() {
        return [SELECT Name FROM BuildingSpecifications__c];
    }

    @AuraEnabled
    public static List<Floor_Specification__c> getFloorSpecifications() {
        return [SELECT Name FROM Floor_Specification__c];
    }

    class RoomWrapper {

        Id roomId;
        Id contentId;
        Id picId;
        String url;

        public RoomWrapper(Id roomId, Id contentId) {
            this.roomId = roomId;
            this.contentId = contentId;
        }

        public RoomWrapper(Id roomId) {
            this.roomId = roomId;
        }
    }

    @AuraEnabled
    public static String getMainPictures(List<String> roomsId) {

        List<RoomWrapper> wrappers = new List<ToRentComponentController.RoomWrapper>();
        Set<Id> contentIds = new Set<Id>();
        List<Id> linkedEntityIds = new List<Id>();

        Map<Id, List<ContentVersion>> roomIdToContentId = new Map<Id, List<ContentVersion>>();

        for (String item : roomsId) {
            wrappers.add(new RoomWrapper(Id.valueOf(item)));
        }

        List<ContentDocumentLink> contentDocumentLinks = [
                SELECT
                        ContentDocumentId, LinkedEntityId
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :roomsId
        ];

        for (RoomWrapper item : wrappers) {
            for (ContentDocumentLink link : contentDocumentLinks) {

                if (link.LinkedEntityId == item.roomId) {
                    item.contentId = link.ContentDocumentId;
                    contentIds.add(link.ContentDocumentId);
                }
            }
        }

        List<ContentVersion> content = [
                SELECT
                        Id, Description, ContentDocumentId, Title, FirstPublishLocationId
                FROM ContentVersion
                WHERE ContentDocumentId IN :contentIds
        ];

        for (RoomWrapper wrapper : wrappers) {
            for (ContentVersion item : content) {

                if (wrapper.roomId == item.FirstPublishLocationId) {

                    if (roomIdToContentId.containsKey(wrapper.roomId)) {
                        roomIdToContentId.get(wrapper.roomId).add(item);
                    } else {
                        List<ContentVersion> ids = new List<ContentVersion>();
                        ids.add(item);
                        roomIdToContentId.put(wrapper.roomId, ids);
                    }
                }
            }
        }

        for (RoomWrapper wrapper : wrappers) {
            if (roomIdToContentId.containsKey(wrapper.roomId)) {

                Boolean tmp = true;
                for (ContentVersion item : roomIdToContentId.get(wrapper.roomId)) {
                    if (tmp) {
                        if (item.Description == 'mainPicture') {

                            wrapper.picId = item.Id;
                            wrapper.url = Url.getSalesforceBaseUrl().toExternalForm() +
                                    '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Png&versionId=' + item.Id;

                            tmp = false;
                        } else {
                            Id picId = roomIdToContentId.get(wrapper.roomId)[0].Id;
                            wrapper.url = Url.getSalesforceBaseUrl().toExternalForm() +
                                    '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Png&versionId=' +
                                    picId;
                            wrapper.picId = picId;
                        }
                    }
                }
            }
        }

        for (RoomWrapper wrapper : wrappers) {
            if (wrapper.url == null) {
                wrapper.url = '/resource/No_Image';
            }
        }

        return JSON.serialize(wrappers);
    }

    @AuraEnabled
    public static List<ContentVersion> getRoomPictures(String roomId) {
        return getContent(roomId);

    }

    private static List<ContentVersion> getContent(String roomId) {
        System.debug(roomId);

        List<ContentDocumentLink> contentDocumentLinks = [
                SELECT
                        ContentDocumentId, LinkedEntityId
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :Id.valueOf(roomId)
        ];
        Set<Id> contentIds = new Set<Id>();

        for (ContentDocumentLink item : contentDocumentLinks) {
            contentIds.add(item.ContentDocumentId);
        }

        List<ContentVersion> content = [
                SELECT
                        Id, Description, ContentDocumentId, Title
                FROM ContentVersion
                WHERE ContentDocumentId IN :contentIds
        ];

        return content;
    }

    @AuraEnabled
    public static void setMainPictures(String roomId, String picId) {
        List<ContentVersion> content = getContent(roomId);
        System.debug(content);
        for (ContentVersion item : content) {
            if (item.ContentDocumentId == picId) {
                System.debug(item);
                item.Description = 'mainPicture';
            } else {
                item.Description = '';
            }
        }

        update content;
    }

    @AuraEnabled
    public static void deletePicture(String pictureId) {
        List<ContentDocument> pictures = [
                SELECT Id
                FROM ContentDocument
                WHERE Id = :pictureId
        ];

        delete pictures;
    }

    @AuraEnabled
    public static Id saveBuildings(Account building) {
        insert building;

        return building.Id;
    }

    @AuraEnabled
    public static Map<String, Id> saveSpecifications(List<Specification__c> specifications) {
        Map<String, Id> specificationNameToID = new Map<String, Id>();

        List<String> specificationsName = new List<String>();
        List<String> existSpecificationsName = new List<String>();
        List<Specification__c> nonExistSpecifications = new List<Specification__c>();

        for (Specification__c specification : specifications) {
            specificationsName.add(specification.Name);
        }

        List<Specification__c> existSpecifications = [
                SELECT
                        Id, Name
                FROM Specification__c
                WHERE Name IN :specificationsName
        ];

        for (Specification__c specification : existSpecifications) {
            existSpecificationsName.add(specification.Name);

            if (specificationsName.contains(specification.Name)) {
                specificationNameToID.put(specification.Name, specification.Id);
            }
        }

        for (Specification__c specification : specifications) {
            if (existSpecificationsName.contains(specification.Name) == false) {
                nonExistSpecifications.add(specification);
            }
        }


        insert nonExistSpecifications;


        for (Specification__c item : nonExistSpecifications) {
            specificationNameToID.put(item.Name, item.Id);
        }

        return specificationNameToID;
    }

    @AuraEnabled
    public static void saveSpecificationsJunction(List<Specification_Junction__c> specifications) {
        insert specifications;
    }

    @AuraEnabled
    public static Id saveFloor(Id accountId, String floorNumber) {
        Floor__c floor = new Floor__c(Name = floorNumber, Account__c = accountId);

        insert floor;
        return floor.id;
    }

    @AuraEnabled
    public static List<Object> getCSSpecifications(String roomType) {
        List<Object> specifications;

        if (roomType == 'Room') {
            specifications = new List<RoomSpecifications__c>([SELECT Name FROM RoomSpecifications__c]);
        } else if (roomType == 'Garage') {
            specifications = new List<GarageSpecifications__c>([SELECT Name FROM GarageSpecifications__c]);
        } else if (roomType == 'Something Else') {
            specifications = new List<AnotherSpecifications__c>([SELECT Name FROM AnotherSpecifications__c]);
        }

        return specifications;
    }

    @AuraEnabled
    public static Id saveRoom(Room__c room) {
        insert room;

        return room.Id;
    }

    class NonApprovedObjects {
        List<Account> buildings;
        List<Floor__c> floors;
        List<Room__c> rooms;

        public NonApprovedObjects(List<Account> buildings, List<Floor__c> floors, List<Room__c> rooms) {
            this.buildings = new List<Account>(buildings);
            this.floors = new List<Floor__c>(floors);
            this.rooms = new List<Room__c>(rooms);
        }
    }

    @AuraEnabled
    public static String getNonApprovedObjects() {
        List<Account> buildings = [
                SELECT
                        Name, BillingCountry, BillingCity, BillingStreet, Status__c
                FROM Account
                WHERE Status__c != 'Approved'
        ];

        List<Floor__c> floors = [
                SELECT
                        Name, Status__c, Account__r.Name
                FROM Floor__c
                WHERE Status__c != 'Approved'
        ];

        List<Room__c> rooms = [
                SELECT
                        Name, Type__c, Price__c, Hourly_Wage__c, Description__c, Status__c,
                        Floor__r.Name, Floor__r.Account__r.Name
                FROM Room__c
                WHERE Status__c != 'Approved'
        ];

        NonApprovedObjects newObjects = new NonApprovedObjects(buildings, floors, rooms);

        return JSON.serialize(newObjects);
    }

    @AuraEnabled
    public static String getPicture(String picId) {

        ContentVersion content = [SELECT Id FROM ContentVersion WHERE ContentDocumentId =: picId LIMIT 1];

        String url = Url.getSalesforceBaseUrl().toExternalForm() +
                '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Png&versionId=' + content.Id;

        return url;
    }

    @AuraEnabled
    public static void setEditedStatusToRoom(String roomId) {
        Room__c room = [SELECT Status__c FROM Room__c WHERE Id =: roomId];

        room.Status__c = 'Edited';

        update room;
    }

    @AuraEnabled
    public static String autocompleteRequest(String searchKey, String type) {
        String url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?'
                + 'input=' + EncodingUtil.urlEncode(searchKey, 'UTF-8') + '&types=' + type + getKey();
        String response = getResponse(url);
        system.debug('Response suggestions***' + response);
        return response;
    }

    private static string getResponse(string strURL) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setMethod('GET');
        req.setEndpoint(strURL);
        req.setTimeout(120000);

        res = h.send(req);
        String responseBody = res.getBody();
        system.debug('responseBody---' + responseBody);
        return responseBody;
    }

    public static String getKey() {
        String key = 'AIzaSyDcNfJbNi-h10foyl5vwlxo0F7kwYySXYM';
        String output = '&key=' + key;
        return output;
    }
}