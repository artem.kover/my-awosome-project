public with sharing class DatePickerCmpController {

    @AuraEnabled
    public static Boolean isHourlyWage(String id) {
        Room__c room = [SELECT Hourly_Wage__c FROM Room__c WHERE Id =:id LIMIT 1];

        return room.Hourly_Wage__c;
    }

    @AuraEnabled
    public static string getBookingDate(String id){
        return JSON.serialize([
                SELECT
                    Id,
                    Room__r.Id,
                    Room__r.Hourly_Wage__c,
                    Room__r.Name,
                    Start_Date_Time__c,
                    End_Date_Time__c
                FROM
                    Booking_Information__c
                WHERE
                    Room__r.Id=:Id AND Start_Date_Time__c >= TODAY
                ORDER BY
                    Start_Date_Time__c
        ]);
    }

}