/**
 * Created by Artem Koverchik on 15.01.2020.
 */

public with sharing class DeleteNonApprovedRecordsBatch implements Database.Batchable<SObject> {

    public Database.queryLocator start(Database.BatchableContext context) {

        String query = '' +
                'SELECT Id FROM Room__c WHERE Status__c = \'Rejected\' AND LastModifiedDate >=: System.today() - 7';

        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext context, List<Room__c> rooms) {

        List<Id> roomsId = new List<Id>();
        Set<Id> contentIds = new Set<Id>();

        List<Account> buildings = [
                SELECT Id
                FROM Account
                WHERE Status__c = 'Rejected'
                AND LastModifiedDate >=: System.today() - 7
        ];

        List<Floor__c> floors = [
                SELECT Id
                FROM Floor__c
                WHERE Status__c = 'Rejected'
                AND LastModifiedDate >=: System.today() - 7
        ];

        List<Specification__c> specifications = [
                SELECT Id
                FROM Specification__c
                WHERE Status__c = 'Rejected'
                AND LastModifiedDate >=: System.today() - 7
        ];

        for (Room__c room : rooms) {
            roomsId.add(room.Id);
        }

        List<ContentDocumentLink> contentDocumentLinks = [
                SELECT
                        ContentDocumentId
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :roomsId
        ];

        for (ContentDocumentLink item : contentDocumentLinks) {
            contentIds.add(item.ContentDocumentId);
        }

        List<ContentDocument> content = [
                SELECT
                        Id
                FROM ContentDocument
                WHERE Id IN :contentIds
        ];

        delete content;
        delete buildings;
        delete floors;
        delete rooms;
        delete specifications;
    }

    public void finish(Database.BatchableContext context) {

    }

}