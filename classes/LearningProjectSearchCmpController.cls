public with sharing class LearningProjectSearchCmpController {
    
    @AuraEnabled
    public static List<String> getUrlImages() {
        List<ContentVersion> contentVersions = [SELECT Id, FirstPublishLocation.Name FROM ContentVersion WHERE FirstPublishLocationId = 'a022w000000ETjrAAG'];
        List<String> urls = new List<String>();

        for (ContentVersion contentItem : contentVersions) {
            urls.add(URL.getSalesforceBaseUrl().toExternalForm() + '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Png&versionId=' + contentItem.Id);
        }

        return urls;
    }

    public class RoomWrapper {
        public List<String> url {get;set;}
        public String accountName {get;set;}
        public String billingCountry {get;set;}
        public String billingCity {get;set;}
        public String billingStreet {get;set;}
        public String roomName {get;set;}
        public String description {get;set;}
        public Decimal priceRoom {get;set;}
        public Boolean hourlyWage {get;set;}
        public Decimal rating {get;set;}
    }

    @AuraEnabled
    public static String getRooms(String conditions, String typeOfRoom, String sortBy) {
        List<Room__c> rooms = new List<Room__c>();
        if(conditions == '' && typeOfRoom == 'Booking') {
            List<String> bookingRooms = getAllBookingRooms();
            rooms = [
                    SELECT
                            Id, Name, Description__c, Price__c, Hourly_Wage__c, Average_Rating__c,
                            Floor__r.Account__r.Id, Floor__r.Account__r.BillingCity, Floor__r.Account__r.BillingCountry,
                            Floor__r.Account__r.BillingStreet, Floor__r.Account__r.Name
                    FROM Room__c
                    WHERE ID IN :bookingRooms
            ];
        } else if(conditions == '') {
            String soqlString = 'SELECT ' +
                            'Id, Name, Description__c, Price__c, Hourly_Wage__c, Average_Rating__c, ' +
                            'Floor__r.Account__r.Id, Floor__r.Account__r.BillingCity, Floor__r.Account__r.BillingCountry, ' +
                            'Floor__r.Account__r.BillingStreet, Floor__r.Account__r.Name ' +
                    'FROM Room__c ' + sortBy;

            rooms = Database.query(soqlString);
        } else {
            
            rooms = getIdRooms(conditions, typeOfRoom);
        }
        

        List<ContentVersion> contents = [
            SELECT Id, Description, FirstPublishLocation.Name 
            FROM ContentVersion 
            WHERE Description = 'Preview' AND FirstPublishLocationId IN :rooms
        ];

        List<RoomWrapper> roomWrapper = new List<RoomWrapper>();

        for(Room__c roomItem : rooms) {
            RoomWrapper roomWrapperItem = new RoomWrapper();

            roomWrapperItem.accountName = roomItem.Floor__r.Account__r.Name;
            roomWrapperItem.billingCountry = roomItem.Floor__r.Account__r.BillingCountry;
            roomWrapperItem.billingCity = roomItem.Floor__r.Account__r.billingCity;
            roomWrapperItem.billingStreet = roomItem.Floor__r.Account__r.BillingStreet;
            roomWrapperItem.roomName = roomItem.Name;
            roomWrapperItem.hourlyWage = roomItem.Hourly_Wage__c;
            roomWrapperItem.description = roomItem.Description__c;
            roomWrapperItem.rating = roomItem.Average_Rating__c;
            
            if(roomItem.Hourly_Wage__c == true) {
                roomWrapperItem.priceRoom = roomItem.Price__c;
            } else if(roomItem.Price__c != null) {
                roomWrapperItem.priceRoom = roomItem.Price__c * 24;
            }

            List<String> urls = new List<String>();

            for(ContentVersion contentItem : contents) {
                if(roomItem.Id == contentItem.FirstPublishLocationId) {
                    urls.add(URL.getSalesforceBaseUrl().toExternalForm() + '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Png&versionId=' + contentItem.Id);
                }
            }

            roomWrapperItem.url = urls;

            roomWrapper.add(roomWrapperItem);
        }

        return JSON.serialize(roomWrapper);
    }

    public class SpecificationWrapper {
        public String sObjectName {get;set;}
        public String fieldName {get;set;}
        public String type {get;set;}
    }

    @AuraEnabled
    public static string getSpecifications(){
        List<Specification__c> specifications = [SELECT Name, Type__c, (SELECT Account__r.Id, Floor__r.Id, Room__r.Id, Number_Value__c, Text_Value__c, Boolean_Value__c FROM Specification_Junction__r) FROM Specification__c];
        List<SpecificationWrapper> specificationWrapper = new List<SpecificationWrapper>();
        List<String> specificationsAccounts = new List<String>();
        List<String> specificationsFloors = new List<String>();
        List<String> specificationsRooms = new List<String>();
        for (Specification__c specification : specifications) {

            for(Specification_Junction__c specificationJunctionItem : specification.Specification_Junction__r) {

                if(specificationJunctionItem.Account__r.Id != null) {
                    SpecificationWrapper specificationWrapperItem = new SpecificationWrapper();
                    specificationWrapperItem.sObjectName = 'account';

                    if(specificationJunctionItem.Number_Value__c != null) {
                        specificationWrapperItem.fieldName = specification.Name;
                        specificationWrapperItem.type = 'number';
                    } else if(specificationJunctionItem.Text_Value__c != null) {
                        specificationWrapperItem.fieldName = specification.Name;
                        specificationWrapperItem.type = 'text';
                    } else {
                        specificationWrapperItem.fieldName = specification.Name;
                        specificationWrapperItem.type = 'checkbox';
                    }

                    specificationWrapper.add(specificationWrapperItem);
                }

                if(specificationJunctionItem.Floor__r.Id != null) {
                    SpecificationWrapper specificationWrapperItem = new SpecificationWrapper();
                    specificationWrapperItem.sObjectName = 'floor';
                    
                    if(specificationJunctionItem.Number_Value__c != null) {
                        specificationWrapperItem.fieldName = specification.Name;
                        specificationWrapperItem.type = 'number';
                    } else if(specificationJunctionItem.Text_Value__c != null) {
                        specificationWrapperItem.fieldName = specification.Name;
                        specificationWrapperItem.type = 'text';
                    } else {
                        specificationWrapperItem.fieldName = specification.Name;
                        specificationWrapperItem.type = 'checkbox';
                    }

                    specificationWrapper.add(specificationWrapperItem);
                }

                if(specificationJunctionItem.Room__r.Id != null) {
                    SpecificationWrapper specificationWrapperItem = new SpecificationWrapper();
                    specificationWrapperItem.sObjectName = 'room';
                    
                    if(specificationJunctionItem.Number_Value__c != null) {
                        specificationWrapperItem.fieldName = specification.Name;
                        specificationWrapperItem.type = 'number';
                    } else if(specificationJunctionItem.Text_Value__c != null) {
                        specificationWrapperItem.fieldName = specification.Name;
                        specificationWrapperItem.type = 'text';
                    } else {
                        specificationWrapperItem.fieldName = specification.Name;
                        specificationWrapperItem.type = 'checkbox';
                    }

                    specificationWrapper.add(specificationWrapperItem);
                }
            }

        }

        List<SpecificationWrapper> sortedSpecificationWrapper = new List<SpecificationWrapper>();

        for(integer i = 0; i < specificationWrapper.size(); i++) {
            for(integer j = 0; j < specificationWrapper.size(); j++) {
                if(specificationWrapper[i].sObjectName == specificationWrapper[j].sObjectName && j != i && specificationWrapper[i].fieldName == specificationWrapper[j].fieldName && specificationWrapper[i].type == specificationWrapper[j].type) {
                    specificationWrapper.remove(i);
                    i--;
                    break;
                }
            }
        }
        System.debug(specificationWrapper);
        return JSON.serialize(specificationWrapper);
    }

    public static List<Room__c> getIdRooms(String conditions, String typeOfRoom) {
        Map<String, String> specificationMap = getAllSpecificationMap(conditions);
        System.debug('typeof room');
        System.debug(getAllSpecificationMap(conditions));
        System.debug(new List<String>(specificationMap.keySet()));
        System.debug(getAllSpecifications(new List<String>(specificationMap.keySet())));
        List<Specification_Junction__c> allSpecifications = getAllSpecifications(new List<String>(specificationMap.keySet()));
        List<Specification_Junction__c> specificationsAccount = getSortedSpecifications(allSpecifications, 'Account');
        List<Specification_Junction__c> specificationsFloor = getSortedSpecifications(allSpecifications, 'Floor');
        List<Specification_Junction__c> specificationsRoom = getSortedSpecifications(allSpecifications, 'Room');
        Map<String, String> accountMap = getSpecificationMap(specificationsAccount, specificationMap);
        Map<String, String> floorMap = getSpecificationMap(specificationsFloor, specificationMap);
        Map<String, String> roomMap = getSpecificationMap(specificationsRoom, specificationMap);
        List<String> accountsId = getIdSObjects(specificationsAccount, accountMap, 'Account');
        List<String> floorsId = getIdSObjects(specificationsFloor, floorMap, 'Floor');
        List<String> roomsId = getIdSObjects(specificationsRoom, roomMap, 'Room');
        System.debug(accountMap);
        System.debug(floorMap);
        System.debug(roomMap);
        System.debug(accountsId);
        System.debug(floorsId);
        System.debug(roomsId);
        String accountName = '';
        if(specificationMap.containsKey('searchName') == true) {
            accountName = specificationMap.get('searchName');
        }
        System.debug(accountName);
        // if(specificationMap.containsKey('startDate') == true && specificationMap.containsKey('endDate') == true) {
        //     roomsId = getNamesNotBookingRooms(roomsId, getBookingRoomsBetweenDates(specificationMap.get('startDate'),specificationMap.get('endDate')));
        // }

        // System.debug(roomsId);


        Boolean isValidAccount = accountMap.size() != 0 && accountsId.size() != 0 || accountMap.size() == 0 && accountsId.size() == 0;
        Boolean isValidFloor = floorMap.size() != 0 && floorsId.size() != 0 || floorMap.size() == 0 && floorsId.size() == 0;
        Boolean isValidRoom = roomMap.size() != 0 && roomsId.size() != 0 || roomMap.size() == 0 && roomsId.size() == 0;
        if (isValidAccount == true && isValidFloor == true && isValidRoom == true) {
            if(specificationMap.containsKey('searchStartDate') == true && specificationMap.containsKey('searchEndDate') == true) {
                System.debug('search start and end');
                return getNamesNotBookingRooms(getRoomsWithConditions(accountsId, floorsId, roomsId, accountName, typeOfRoom), getBookingRoomsBetweenDates(specificationMap.get('searchStartDate'),specificationMap.get('searchEndDate')));
            } else if(specificationMap.containsKey('searchStartDate') == true && specificationMap.containsKey('hourlyWage') == true) {
                System.debug('hourlyWage ! true');
                return getNamesNotBookingRooms(getRoomsWithConditions(accountsId, floorsId, roomsId, accountName, typeOfRoom), getBookingBetweenDates(specificationMap.get('searchStartDate')));
            } else {
                System.debug('else');
                return getRoomsWithConditions(accountsId, floorsId, roomsId, accountName, typeOfRoom);
            }
        } else {
            return new List<Room__c>();
        }
        // System.debug(accountMap.size() != 0 && accountsId.size() != 0 || accountMap.size() == 0 && accountsId.size() == 0);
        // System.debug(floorMap.size() != 0 && floorsId.size() != 0 || floorMap.size() == 0 && floorsId.size() == 0);
        // System.debug(roomMap.size() != 0 && roomsId.size() != 0 || roomMap.size() == 0 && roomsId.size() == 0);
    }

    public static List<Room__c> getNamesNotBookingRooms(List<Room__c> rooms, List<String> bookingRooms) {
        List<Room__c> uniqueRooms = new List<Room__c>();
        for(integer i = 0; i < rooms.size(); i++) {
            integer count = 0;

            for(integer j = 0; j < bookingRooms.size(); j++) {
                if(rooms[i].Id != bookingRooms[j]) {
                    count++;
                }
            }
            
            if(count == bookingRooms.size()) {
                uniqueRooms.add(rooms[i]);
            }
        }
        System.debug(rooms);
        System.debug(bookingRooms);
        System.debug(uniqueRooms);
        return uniqueRooms;
    }

    public static List<String> getBookingRoomsBetweenDates(String startDate, String endDate) {
        List<String> rooms = new List<String>();
        
        List<String> smth1 = startDate.split('-');
        startDate = smth1[1] + '/' + smth1[2] + '/' + smth1[0];

        List<String> smth2 = endDate.split('-');
        endDate = smth2[1] + '/' + smth2[2] + '/' + smth2[0];

        Date firstDate = date.parse(startDate);
        Date secondDate = date.parse(endDate);

        List<Booking_Information__c> booking = [SELECT Start_Date_Time__c, End_Date_Time__c, Room__r.Id FROM Booking_Information__c LIMIT 10000];
        for(Integer i = 0; i < booking.size(); i++) {
            if(!(booking[i].Start_Date_Time__c > secondDate || booking[i].End_Date_Time__c < firstDate)) {
                rooms.add(booking[i].Room__r.Id);
            }
        }
        System.debug('ROOMS' + rooms);
        return rooms;
    }

    private static Map<String,String> getAllSpecificationMap(String conditions) {
        List<String> conditionsList = conditions.split('%');
        Map<String, String> conditionMap = new Map<String, String>();

        for(Integer i = 0; i < conditionsList.size(); i++) {
            List<String> conditionItem = conditionsList[i].split('=');
            conditionMap.put(conditionItem[0],conditionItem[1]);
        }

        return conditionMap;
    }

    private static Map<String, String> getSpecificationMap(List<Specification_Junction__c> specifications, Map<String, String> allSpecificationMap) {
        Map<String, String> specificationMap = new Map<String, String>();

        for (Specification_Junction__c specificationItem: specifications) {
            for (String key: allSpecificationMap.keySet()) {
                if (key == specificationItem.Specification__r.Name) {
                    specificationMap.put(key, allSpecificationMap.get(key));
                }
            }
        }

        return specificationMap;
    }

    // private static Map<String,String> getSpecifications()

    private static List<Specification_Junction__c> getSortedSpecifications(List<Specification_Junction__c> specifications, String objectSpeficiation) {
        List<Specification_Junction__c> specification = new List<Specification_Junction__c>();

        // for(Specification__c specificationItem : specifications) {
        //     for(Specification_Junction__c specificationJunctionItem : specificationItem.Specification_Junction__r) {
        //         if(specificationJunctionItem.Account__r.Id != null && objectSpeficiation == 'Account') {
        //             specification.add(specificationJunctionItem);
        //         } else if(specificationJunctionItem.Floor__r.Id != null && objectSpeficiation == 'Floor') {
        //             specification.add(specificationJunctionItem);
        //         } else if(specificationJunctionItem.Room__r.Id != null && objectSpeficiation == 'Room') {
        //             specification.add(specificationJunctionItem);
        //         }
        //     }
        // }

        for(Specification_Junction__c specificationJunctionItem : specifications) {
            if(specificationJunctionItem.Account__r.Id != null && objectSpeficiation == 'Account') {
                specification.add(specificationJunctionItem);
            } else if(specificationJunctionItem.Floor__r.Id != null && objectSpeficiation == 'Floor') {
                specification.add(specificationJunctionItem);
            } else if(specificationJunctionItem.Room__r.Id != null && objectSpeficiation == 'Room') {
                specification.add(specificationJunctionItem);
            }
        }

        return specification;
    }

    public static List<Specification_Junction__c> getAllSpecifications(List<String> specifications) {
        return [SELECT Account__r.Id, Floor__r.Id, Floor__r.Account__c, Room__r.Id, Specification__r.Name,  Room__r.Floor__c, Number_Value__c, Text_Value__c, Boolean_Value__c FROM Specification_Junction__c WHERE Specification__r.Name IN :specifications];
           
        
        
        // return new List<Specification_Junction__c>();
        // return [SELECT Name, Type__c ,(SELECT Account__r.Id, Floor__r.Id, Floor__r.Account__c, Room__r.Id, Specification__r.Name,  Room__r.Floor__c, Number_Value__c, Text_Value__c, Boolean_Value__c FROM Specification_Junction__r) FROM Specification__c WHERE Name IN :specifications];
    }

    private static List<String> getIdSObjects(List<Specification_Junction__c> specifications, Map<String, String> specificationMap, String sObjectName) {
        List<String> idSObjects = new List<String>();
        Map<String, Integer> smth = new Map<String, Integer>();
        if (specifications.size() != 0) {
            for (Specification_Junction__c specificationItem: specifications) {
                for (String key: specificationMap.keySet()) {
                    if (key == specificationItem.Specification__r.Name) {
                        if(specificationItem.Boolean_Value__c != null && specificationItem.Boolean_Value__c != Boolean.valueOf(specificationMap.get(key))) {
                            break;
                        } else if(specificationItem.Number_Value__c != null && specificationItem.Number_Value__c != Integer.valueOf(specificationMap.get(key))) {
                            break;
                        } else if(specificationItem.Text_Value__c != null && specificationItem.Text_Value__c != String.valueOf(specificationMap.get(key))) {
                            break;
                        }
                        if(smth.containsKey(specificationItem.Room__r.Id) == false) {
                            if(sObjectName == 'Account') {
                              smth.put(specificationItem.Account__r.Id, 1);  
                            } else if(sObjectName == 'Floor') {
                              smth.put(specificationItem.Floor__r.Id, 1);  
                            } else if(sObjectName == 'Room') {
                              smth.put(specificationItem.Room__r.Id, 1);  
                            }
                        } else {
                            if(sObjectName == 'Account') {
                              smth.put(specificationItem.Account__r.Id, smth.get(specificationItem.Account__r.Id) + 1);
                            } else if(sObjectName == 'Floor') {
                              smth.put(specificationItem.Floor__r.Id, smth.get(specificationItem.Floor__r.Id) + 1);  
                            } else if(sObjectName == 'Room') {
                              smth.put(specificationItem.Room__r.Id, smth.get(specificationItem.Room__r.Id) + 1);
                            }
                        }
                    }
                }
            }
        }
        for(String smthItem : smth.keySet()) {
            if(smth.get(smthItem) == specificationMap.size()) {
                idSObjects.add(smthItem);
            } 
        }

        return idSObjects;
    }

    private static List<Room__c> getRoomsWithConditions(List<String> accounts, List<String> floors, List<String> rooms, String accountName, String typeOfRoom) {
        List<Room__c> roomsList;
        String userId = UserInfo.getUserId();
        String query = 'SELECT Id, Name, Description__c, Price__c, Hourly_Wage__c, Floor__r.Account__r.Id, Floor__r.Account__r.BillingCity, Floor__r.Account__r.BillingCountry, Floor__r.Account__r.BillingStreet, Floor__r.Account__r.Name, Average_Rating__c FROM Room__c ';
        String startQuery = query;



        if(typeOfRoom == 'Booking') {
            if(rooms.size() == 0) {
                if(query == startQuery) {
                    rooms = getAllBookingRooms();
                    System.debug(rooms);
                    query += ' WHERE Id IN :rooms';
                } else if(query != startQuery) {
                    rooms = getAllBookingRooms();
                    System.debug(rooms);
                    query += ' AND Id IN :rooms';
                }
            } else if(rooms.size() != 0) {
                if(query == startQuery) {
                    rooms = getBookingRooms(rooms);
                    System.debug(rooms);
                    query += ' WHERE Id IN :rooms';
                } else if(query != startQuery) {
                    rooms = getBookingRooms(rooms);
                    System.debug(rooms);
                    query += ' AND Id IN :rooms';
                }
            }

            System.debug(getAllBookingRooms());
            System.debug(getBookingRooms(rooms));
        } else {
            if(query == startQuery && rooms.size() != 0) {
                query += ' WHERE Id IN :rooms';
            } else if(query != startQuery && rooms.size() != 0) {
                query += ' AND Id IN :rooms';
            }
        }

        if(query == startQuery && accounts.size() != 0) {
            query += ' WHERE Floor__r.Account__r.Id IN :accounts';
        } else if(query != startQuery && accounts.size() != 0) {
            query += ' AND Floor__r.Account__r.Id IN :accounts';
        }

        if(query == startQuery && floors.size() != 0) {
            query += ' WHERE Floor__r.Id IN :floors';
        } else if(query != startQuery && floors.size() != 0) {
            query += ' AND Floor__r.Id IN :floors';
        }

        if(query == startQuery && accountName != '') {
            query += ' WHERE Name =:accountName';
        } else if(query != startQuery && accountName != '') {
            query += ' AND Name =:accountName';
        }

        if(typeOfRoom == 'MyRooms') {
            if(typeOfRoom == 'MyRooms' && startQuery == query) {
                query += ' WHERE CreatedById =:userId';
            } else if(typeOfRoom == 'MyRooms' && startQuery != query) {
                query += ' AND CreatedById =:userId';
            }
        }

        System.debug(query);
        System.debug(Database.query(query));
        // return new List<Room__c>();
        return Database.query(query);

    }

    private static List<String> getBookingRooms(List<String> rooms) {
        String userId = UserInfo.getUserId();
        List<Booking_Information__c> booking = [SELECT Id, Room__r.Id FROM Booking_Information__c WHERE CreatedById=:userId AND Room__r.Id IN :rooms];
        Set<String> sortedRooms = new Set<String>();
        
        for(Booking_Information__c bookingItem : booking) {
            sortedRooms.add(bookingItem.Room__r.Id);
        }
        
        return new List<String>(sortedRooms);
    }

    private static List<String> getAllBookingRooms() {
        String userId = UserInfo.getUserId();
        List<Booking_Information__c> booking = [SELECT Id, Room__r.Id FROM Booking_Information__c WHERE CreatedById=:userId];
        Set<String> rooms = new Set<String>();
        
        for(Booking_Information__c bookingItem : booking) {
            rooms.add(bookingItem.Room__r.Id);
        }
        
        return new List<String>(rooms);
    }

    @AuraEnabled
    public static String autocompleteRequest(String searchKey) {
        String url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?' + 'input=' + EncodingUtil.urlEncode(searchKey, 'UTF-8') + '&types=address'  + getKey();
        String response = getResponse(url);
        system.debug('Response suggestions***'+response);
        return response;
    }

    private static string getResponse(string strURL){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setMethod('GET');
        req.setEndpoint(strURL);
        req.setTimeout(120000);

        res = h.send(req);
        String responseBody = res.getBody();
        system.debug('responseBody---'+responseBody);
        return responseBody;
    }

    public static String getKey(){
        //*Enter your API key here*//
        String key = 'AIzaSyDcNfJbNi-h10foyl5vwlxo0F7kwYySXYM';
        String output = '&key=' + key;
        return output;
    }

    public static List<Booking_Information__c> getBookingBetweenDates(DateTime startDate, DateTime endDate) {
        return [SELECT Start_Date_Time__c, End_Date_Time__c, Room__r.Id FROM Booking_Information__c WHERE Start_Date_Time__c >= :startDate AND End_Date_Time__c <= :endDate LIMIT 10000];
    }

    public static DateTime getDate(String startDate) {
        List<String> dates = startDate.split('-');
        startDate = dates[1] + '/' + dates[2] + '/' + dates[0];
        DateTime firstDate = date.parse(startDate);

        return firstDate.addHours(getTimeZone());
    }

    public static Integer getTimeZone() {
        return UserInfo.getTimezone().getOffset(Datetime.now()) *-1 /1000 / 60 / 60;
    }

    public static List<String> getBookingBetweenDates(String tempDate) {
        DateTime startDate = getDate(tempDate);
        DateTime endDate = startDate.addDays(1);
        List<Booking_Information__c> booking = getBookingBetweenDates(startDate, endDate);
        Integer seconds = 0;
        Map<String, Integer> datesMap = new Map<String, Integer>();

        for (Integer i = 0; i < booking.size(); i++) {
            String strValue = String.valueOf(booking[i].Start_Date_Time__c);
            DateTime startTime = DateTime.valueOf(booking[i].Start_Date_Time__c);
            DateTime endTime = DateTime.valueOf(booking[i].End_Date_Time__c);
            Integer elapsedDays = endTime.day() - startTime.day();
            Integer elapsedHours = endTime.hour() - startTime.hour();
            Integer elapsedMinutes = endTime.minute() - startTime.minute();
            Integer elapsedSeconds = endTime.second() - startTime.second();
            Integer secondsBetweenDates = elapsedDays * 24 * 60 * 60 - elapsedHours * 24 * 60 - elapsedMinutes * 60 - elapsedSeconds;
            seconds += secondsBetweenDates;
            if(datesMap.containsKey(Booking[i].Room__r.Id)) {
                Integer text = datesMap.get(Booking[i].Room__r.Id);
                text += secondsBetweenDates;
                datesMap.put(Booking[i].Room__r.Id, text);
            } else {
                datesMap.put(Booking[i].Room__r.Id, secondsBetweenDates);
            }
        }

        List<String> rooms = new List<String>();
        for(String roomItem : datesMap.keySet()) {
            if(datesMap.get(roomItem) >= 86400) {
                rooms.add(roomItem);
            }
        }
        System.debug(datesMap);
        System.debug(rooms);
        return rooms;
    }

}
